# matlab_packages

A set of useful packages for MATLAB

Each directory that begins with "+" contains a package. If this package is placed within the
MATLAB path, the functions within it can be accessed using the notation:

  >> package_name.function_name

Packages can contain subpackages which are referenced as might be expected:

  >> package_name.subpackage_name.function_name





Package list:

+atm
+fig
+netcdf
+ncutil
+earth
+mutil
+zbp
+NCI


#########################################################################################################




+atm		################################################################

   Some functions to calculate thermodynamic quantities useful in the atmospheric sciences.
   For instance, calculating saturation humidities, calculating a moist adiabat, and calculating entropy.

   calculate_MSE                  - Function to calculate the Moist static energy
   calculate_adiabat              - Function to calculate an adiabatic parcel ascent with various thermodynamic assumptions
   calculate_dTdp_adiabatic       - Function to calculate the derivative of Temperature with respect to pressure along moist adiabat 
   calculate_dhdp_adiabatic       - Function to calculate the derivative of enthalpy (h) with respect to pressure along a reversible moist adiabat 
   calculate_enthalpy             - Function to calculate the enthalpy
   calculate_entropy              - Function to calculate the entropy
   calculate_frac_ice             - Calculate the fraction of liquid and ice for a saturation adjustment
   calculate_theta_ep             - Calculate pseudo-equivelant potential temperature
   desatdT                        - Function to calculate the derivative of the saturation vapor pressure
   e_sat                          - Function to calculate saturation vapor pressure
   invert_enthalpy                - Invert enthalpy to give temperature given total water content and pressure
   invert_theta_ep                - Invert the pseudo-equivelant potetial temperature
   load_constants                 - Load a table of constants for thermodynamic calculations
   q_sat                          - Function to calculate saturation specific humidity
   r_sat                          - Function to calculate saturation mixing ratio
   saturation_adjustment          - Function to calculate the breakdown of water into vapor, liquid and solid 
   test_script                    - Script to perform some tests to ensure these scripts are working as intended



+zbp		################################################################

   Package to calculate the zero buoyancy plume model of Singh & O'Gorman (2013)
   Uses thermodynamic functions from +atm. Stand-alone function also available
   at Martin Singh's website.

   calculate_plume		  - Calculates zero-buoyancy plume model solution for given cloud-base parameters



+fig		################################################################

   Some useful plotting functions


   arrow                          - Draw a line with an arrowhead.
   bfig                           - Function to create a figure with specified width and height
   drawbrace                      - Draw curly brace on current figure
   red2blue                       - Function to make a red-blue anomaly colormap 
   format_ticks			  - Handy function to add degree symbols (or other symbols) to axis ticks
   set_default			  - Set the defaults for different types of figures (e.g., paper, talk, etc.)



+netcdf		################################################################

   Functions for netcdf files

   time                           - Function that returns time information from NETCDF fiels that conform to COARDS standards
   ncload			  - Load all variables in netcdf file into workspace 


+earth		###############################################################

   Functions related to the Earth

   ls_mask                        - Gives a land mask at given resolution
   area_mean  			  - caluclate an area-weighted mean over an area of a latitude-longitude box
   coasts			  - get the coastline data
   const			  - some constants related to the Earth (not thermodynamic)


+mutil		###############################################################

   Matrix utilities  

   smooth2 			  - Smooths 2D array data.  Ignores NaN's.
   crossing                       - Find the crossings of a given level of a signal
   conseq			  - Find the length of consequtive ones in a matrix
   cumsimps			  - perform cumulative integral using Simpson's rule
 

+NCI		###############################################################

   Some functions usful for use on the NCI supercomputer raijin

   +CMIP5			  - some functions for getting CMIP5 data from raijin
        CMIP5_metadata
        check_filenames
	check_time
        check_data
        get_axes
        get_exp_times
        get_field
        get_files
        get_ls_lask
        get_time_range
        get_time_vector
        model_info
        plot_model_avail
        save_file_indices
        save_file_indices_mod
        vert_vars

   +ERA				  - some functions for getting ERA data from raijin
        ERA_metadata
        check_time
        get_axes
        get_field
        get_files






