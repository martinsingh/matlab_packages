function [T,p,rho,RH,Gamma,Gamma_m,Mu,Md,z,snet,scond] = calculate_ZBP(T0,p0,z0,zLCL,epsilon,mu,w,z,varargin)
% Zero-buoyancy plume model solution for atmospheric vertical profile
%
% Calculate the ZBP solution given the value of the height of the LCL, the
% temperature at a reference level and the pressure at the same level, or
% over the whole column.
%
% Additional inputs are the entrainment rate, evaporation parameter,
% and the net mass flux.
%
%
%
% [T,p,rho,RH,Gamma,Gamma_m,Mu,Md] = calculate_ZBP(T0,p0,z0,zLCL,epsilon,mu,w,z,delta,Qpar)
%
% INPUTS:
%
% T0 = temperature at reference level       (K)
% p0 = pressure at reference level          (Pa)
%      or pressure matrix if given
% z0 = height at reference level            (m)
% zLCL = height of LCL                      (m)
%
% epsilon = entrainment rate                (m^-1)
% mu = evaporation parameter
% w = large-scale vertical velocity         (m s^{-1})
%
% Optional:
% delta = detrainment rate                  (m^-1)
% Qpar = radiative cooling parameters
%
%
%
% OUTPUTS:
%
% T = temperature profile                   (K)
% p = pressure profile                      (Pa)
% rho = density profile                     (rho)
% RH = relative humidity profile            (RH)
% Gamma = lapse rate                        (K m^-1)
% Gamma_m = moist adiabatic lapse rate      (K m^-1)
% Mu = upward mass flux                     (kg m^-2)
% Md = downward mass flux                   (kg m^-2)

%% Load the thermodynamics parameters
c = atm.load_constants;

%% Optional arguments - mass fluxes and detrainment rate
psolve = 1;
delta = epsilon;
Qpar.Q = -1;
Qpar.Tt = 0;
Qpar.Tm = 100;
ent_type = 'const';

% Set or Calculate the detrainment
if nargin >=      9; delta = varargin{1}; end
if nargin >=     10; Qpar = varargin{2}; end
if nargin >=     11; ent_type = varargin{3}; end
T_tropopause = Qpar.Tt;

% Set method to rk4
method = 'rk4';


%% Find the height of particular levels

% Location of the reference level
i0 = find(z==z0);

% If reference level is not in the height vector, add it
if isempty(i0)
    z = [z z0];
    z = sort(z);
    i0 = find(z==zLCL);
end

% Location of the LCL
iLCL = find(z==zLCL);

% If zLCL is not in the height vector, add it
if isempty(iLCL)
    z = [z zLCL];
    z = sort(z);
    iLCL = find(z==zLCL);
end

%% Ensure vectors are correct size

% Set w to the right size
if length(w)     ==1;         w = w.*ones(size(z));         end


% Set pressure if given
if length(p0) == length(z); p = p0; psolve = 0; end

%% Initialise the vectors
N = length(z);

T = zeros(size(z));
RH = nan(size(z));

Gamma = zeros(size(z));
Gamma_m = zeros(size(z));
Mu = nan(size(z));
Md = nan(size(z));

if psolve == 1;  p = zeros(size(z)); end


% Set entrainment rate
if strcmp(ent_type,'const')
    epsilon = epsilon.*ones(size(z));
elseif strcmp(ent_type,'1oz')
    % Entrainment value divided by z in km - input epsilon is the vlue at 1 km.
    epsilon = min(1./zLCL,1./z).*epsilon.*1000;
else
    error('unknown entrainment formulation')
end

if ischar(delta)
    delta = epsilon;
elseif length(delta) ==1
    delta = delta.*ones(size(z));
end

%% Set the reference values
T(i0) = T0;
if psolve; p(i0) = p0; end

%% Integrate from reference level downwards to LCL, and LCL downwards to surface

if z0 > zLCL

    %% Integrate from reference value downwards to LCL
    for i = i0:-1:iLCL

        % Calculate the radiative cooling rate
        Q = zbp.calculate_Qcool(T(i),p(i),Qpar);

        % Calculate lapse rate
        [Gamma(i),Gamma_m(i),gamma,gamma_m,RH(i),Mu(i),Md(i),snet(i),scond(i),r(i)] = zbp.calculate_lapse_rate(T(i),p(i),epsilon(i),w(i),Q,mu,delta(i));

        % Create function handle for the integration
        % Needs to be done in the loop
        dTpdz = @(z,Tp) [-calculate_lapse_rate(Tp(1),exp(Tp(2)),epsilon(i),w(i),Q,mu,delta(i)); -(c.g)./(c.Rd.*Tp(1))];

        % Integrate one step
        Tp_in = [T(i); log(p(i))];
        dz = z(i-1)-z(i);
        [Tp_out,~] = ODE.rk_step(dTpdz,Tp_in,z(i),dz,method);

        if i > iLCL
           T(i-1) = Tp_out(1);
           if psolve; p(i-1) = exp(Tp_out(2)); end
        end

    end
      
    % Set subcloud DSE
    s_subcloud = c.cp.*T(iLCL) + c.g.*zLCL;

    %% Solve subcloud layer

    % Assume fixed dry static energy below LCL
    T(1:iLCL) = (s_subcloud - c.g.*z(1:iLCL) )./c.cp;

    % Integrate upwards and downwards to find pressure
    dlpdz = @(z,lp) -(c.g)./(c.Rd.*(s_subcloud - c.g.*z)./c.cp);

    for i = iLCL:-1:2

      lp_in = log(p(i));
      dz = z(i-1)-z(i);
      [lp_out,~] = ODE.rk_step(dlpdz,lp_in,z(i),dz,method);
      if psolve; p(i-1) = exp(lp_out); end

    end
    
 else

    %% if reference level is below the LCL Solve subcloud layer

    % Calculate dry static energy at reference level
    s_subcloud = c.cp.*T0 + c.g.*z0;

    % Assume fixed dry static energy below LCL
    T(1:iLCL) = (s_subcloud - c.g.*z(1:iLCL) )./c.cp;

    % Integrate upwards and downwards to find pressure
    dlpdz = @(z,lp) -(c.g)./(c.Rd.*(s_subcloud - c.g.*z)./c.cp);

    for i = i0:-1:2

      lp_in = log(p(i));
      dz = z(i-1)-z(i);
      [lp_out,~] = ODE.rk_step(dlpdz,lp_in,z(i),dz,method);
      if psolve; p(i-1) = exp(lp_out); end

    end

    for i = i0:iLCL-1

      lp_in = log(p(i));
      dz = z(i+1)-z(i);
      [lp_out,~] = ODE.rk_step(dlpdz,lp_in,z(i),dz,method);
      if psolve; p(i+1) = exp(lp_out); end
    
    end

    % Set the new reference levels to the LCL
    i0 = iLCL;

end
    

%% Integrate upwards from reference level
for i = i0:N

  % Calculate the radiative cooling rate
  Q = zbp.calculate_Qcool(T(i),p(i),Qpar);


  % Create an isothermal stratosphere
  if T(i) <= T_tropopause
    T(i) = T_tropopause;
    RH(i) = nan;
    Gamma(i) = 0;
    
    % Create function handle for the integration
    % Needs to be done in the loop
    dTpdz = @(z,Tp) [0; -(c.g)./(c.Rd.*Tp(1))];
 
    
    
  else
    % Calculate the lapse rate and relative humidity
    try
       [Gamma(i),Gamma_m(i),gamma,gamma_m,RH(i),Mu(i),Md(i),snet(i),scond(i),r(i)] = zbp.calculate_lapse_rate(T(i),p(i),epsilon(i),w(i),Q,mu,delta(i));
    catch
       disp('s')
    end
  
    % Create function handle for the integration
    % Needs to be done in the loop
    dTpdz = @(z,Tp) [-zbp.calculate_lapse_rate(Tp(1),exp(Tp(2)),epsilon(i),w(i),Q,mu,delta(i)); -(c.g)./(c.Rd.*Tp(1))];
 
  end

  if i<N

     Tp_in = [T(i); log(p(i))];
     dz = z(i+1)-z(i);
     [Tp_out,~] = ODE.rk_step(dTpdz,Tp_in,z(i),dz,method);

     T(i+1) = Tp_out(1);
     if psolve; p(i+1) = exp(Tp_out(2)); end

  end

end

% Calculate density
rho = p./(c.Rd.*T);
