function Qcool = calculate_Qcool(T,p,Qpar)
%
% Calculate radiative cooling (W m^-3) based on the following formula
%
% Q(K/day) = Qrate*f(T,T_tropopuase,T_midtrop)
%
% Q(W/m^3) = Q(K/day)*(rho*cp)/86400


% Load thermodynamic constants
c = atm.load_constants;

Qrate = Qpar.Q;
T_tropopause = Qpar.Tt;
T_midtrop = Qpar.Tm;

% Cooling profile
Qcool = Qrate.*ones(size(T));

I = T>T_tropopause & T<T_midtrop;
Qcool(I) = Qrate.*(0.5 + 0.5.*cos( pi.*(T(I)-T_midtrop)./(T_midtrop-T_tropopause) ) );

% Stratosphere
I = T<=T_tropopause;
Qcool(I) = 0;

% Convert to W/m^-3
Qcool = Qcool.*(c.cp.*p./(c.Rd.*T))./86400;