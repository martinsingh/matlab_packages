function RCE = calculate_RCE_solution(varargin)
%
% Calculate the RCE solution for zero-buoyancy plume model
%
%
% RCE = calculate_RCE_solution(T0_RCE,p0_RCE,z0_RCE,zLCL,epsilon,mu,varying_delta,Qpar,ent_type)
%



%% Default inputs

% Entrainment and evaporation
epsilon = 0.6e-3;
mu = 2;
varying_delta = 1;

% RCE initialisation
T0_RCE = 300;
p0_RCE = 101000;

z0_RCE = 50;
zLCL= 500;

% Radiative cooling parameters
Qpar.Tt = 200;
Qpar.Tm = 250;

% Cooling rate (K/day)
Qpar.Q = -1;

% entrainment profile
ent_type = 'const';

%% Parameters that aren't inputs

%% height matrix
zt = 20000;
dz = 20;


if nargin > 0; T0_RCE = varargin{1}; end
if nargin > 1; p0_RCE = varargin{2}; end
if nargin > 2; z0_RCE = varargin{3}; end
if nargin > 3; zLCL = varargin{4}; end
if nargin > 4; epsilon = varargin{5}; end
if nargin > 5; mu = varargin{6}; end
if nargin > 6; varying_delta = varargin{7}; end
if nargin > 7; Qpar = varargin{8}; end
if nargin > 7; ent_type = varargin{9}; end


%% Set the outputs
RCE = struct;


%% Load constants
c = atm.load_constants('SAM');

%% Create height vector

z = [0:dz:zt z0_RCE zLCL];
z = sort(unique(z));
Nz = length(z);



%% Calculate the RCE profile

% Set delta to epsilon to initialise
if varying_delta > 0
   RCE.delta = epsilon.*ones(size(z));
else
    RCE.delta = 'epsilon';
end

for i_delta = 1:1+varying_delta*5
    
    % Calculate plume solution
    [RCE.T,RCE.p,RCE.rho,RCE.RH,RCE.Gamma,RCE.Gamma_m,RCE.Mu,RCE.Md] = zbp.calculate_ZBP(T0_RCE,p0_RCE,z0_RCE,zLCL,epsilon,mu,0,z,RCE.delta,Qpar,ent_type);
    RCE.M = zeros(size(z));
    RCE.r = ones(size(z));
    RCE.z = z;
    
    % Radiative cooling in K/day
    RCE.Q = zbp.calculate_Qcool(RCE.T,RCE.p,Qpar)./(c.cp.*RCE.rho).*86400;
    
    % Calculate diagnostic detrainment
    RCE.delta_diag = epsilon - 1./RCE.Mu.*gradient(RCE.Mu,z);
    
    if i_delta <= varying_delta*5
        % Santinitise and smooth the calculated detrainment to use in the
        % subsequent calculation
        RCE.delta = zbp.calculate_smoothed_detrainment(RCE.delta_diag,epsilon);
    end
    
end
