function [Gamma,Gamma_m,gamma,gamma_m,RH,Mu,Md,snet,scond,r] = calculate_lapse_rate(T,p,epsilon,varargin)
% Calculate the lapse rate according to the zero-buoyancy plume model
%
% Calculate lapse rate based on given entrainment rate and relative humidity:
%    [Gamma,Gamma_m,gamma,gamma_m] = calculate_lapse_rate(T,p,epsilon,'RH',RH [,Q,mu,delta])
%
% Calculate lapse rate and relative humidity based on given entrainment rate and net mass flux:
%    [Gamma,Gamma_m,gamma,gamma_m,RH,Mu,Md,snet,scond,r] = calculate_lapse_rate(T,p,epsilon [,w,Q,mu,delta])
%
%
% INPUTS
%
% T         = temperature               [K]
% p         = pressure                  [Pa]
% epsilon   = Entrainment rate          [m^-1]
%
% OPTIONAL:
% w         = large-scale vertical velocity     (default 0)        [m s^{-1}]
% Q         = radiative cooling rate            (default -0.01)    [W m^-3]
% mu        = evaporation parameter             (default 0)
% delta     = detrainment rate                  (default epsilon)  [m^-1]
%
%
%
% OUTPUTS
%
% Gamma     = lapse rate                                [K m^-1]
% Gamma_m   = moist adiabatic lapse rate                [K m^-1]
% gamma     = fractonal gradient of specific humidity   [m^-1]
% gamma_m   = gamma along a moist adiabat               [m^-1]
% RH        = relative humidity
% Mu        = upward mass flux                          [kg m^-2 s^-1]
% Md        = downward mass flux                        [kg m^-2 s^-1]
% snet      = net condensation rate                     [kg m^-3 s^-1]
% scond     = gross condensation rate                   [kg m^-3 s^-1]
% r         = mass flux ratio Md/Mu                     

%% Default inputs

% Default to RCE
w = 0;

% Default to Q = 0.01 W/m^3
Q = -0.01;

% Default no evaporation
mu = 0;

% Default delta to be same as entrainment rate
delta = epsilon;

% Default to calculate RH
given_RH = 0;

%% Initialize the outputs that may not be set
RH    = nan;
r     = nan;
snet  = nan;
scond = nan;
Mu    = nan;
Md    = nan;
Gamma = nan;
gamma = nan;

%% Optional parameter parsing

% Read the vertical velocity or relative humidity
if nargin >=4  
    w = varargin{1};    
    
    % Check for given relative humidity case
    if strcmp(w,'RH')
        if nargin ==4; error('No relative humidity given'); end
        RHin = varargin{2};
        given_RH = 1;
    end
end

% Read the additional parameters
if nargin >=5+given_RH;  Q        = varargin{2+given_RH};    end
if nargin >=6+given_RH;  mu       = varargin{3+given_RH};    end
if nargin >=7+given_RH;  delta    = varargin{4+given_RH};    end




%% Derived variables

% Different detrainment rates
deltaT = delta.*(1+mu);
deltaE = mu.*delta;

% Thermodynamics
c = atm.load_constants('SAM');

% Convert input vertical velocity to mass flux
M = p./(c.Rd.*T).*w;

% Use SAM ice formulation and include a mixed-phase range of 20 K
es = atm.e_sat(T,'SAM');
qs = c.eps.*es./(p-es);
f = atm.calculate_frac_ice(T,'SAM');
Lv = c.Lv0.*f + c.Ls0.*(1-f);

% Denominator of the moist adiabatic lapse rate
denom = c.cp + qs.*Lv.^2./(c.Rv.*T.^2);              % J/kg

% Moist adiabatic lapse rate
Gamma_m = c.g .* ( 1 + Lv.*qs./(c.Rd.*T) )./( denom );

% fractional saturation humidity gradient along a most adiabat
gamma_m = c.g./(T).* ( Lv./(c.Rv.*T) - c.cp./c.Rd ) ./ ( denom );


%% Undilute case
if epsilon == 0 

    % Moist adiabatic lapse rates

    % Fractional saturation humiidty gradient
    gamma = gamma_m;

    % Lapse rate
    Gamma = Gamma_m;

    return
    
end
    
%% Variables from Romps (2021)

% Units: m^{-1}
A = Lv./(c.Rv.*T.^2) .* ( c.g.*( 1+qs.*Lv./(c.Rd.*T) ) + qs.*Lv.*epsilon )./denom - c.g./(c.Rd.*T);

% Units: m^{-1}
B = Lv./(c.Rv.*T.^2) .* qs.*Lv.*epsilon ./ denom;
 
% Units: m
E = M.*Lv.*qs./Q;


%% Solve for relative humidity and related parameters

% Specical cases 

if given_RH % Given relative humidity
   
   % Set the RH as the fourth input argument
   RH  = RHin;
   
   % Calculate M based on the relative humidity according to the ZBP
    num = -B./deltaT.*RH.^2 + (A + deltaT)./deltaT.*RH -1;
    denom  = B.* deltaE./deltaT.*RH.^3 ...
           + ( -B./deltaT.*deltaE + (epsilon + deltaE - B)  - ((A+deltaT)./deltaT ).*deltaE ).*RH.^2  ...
           + ( (A + deltaT)./deltaT.*deltaE + deltaE + A + B - 2.*(epsilon + deltaE) ).*RH ...
           + (epsilon - A);
       
    E = num./denom;
    M = E.*Q./(Lv.*qs);

   
elseif Q==0  % if radiative cooling is zero

    if w==0 % If we are in RCE, then ZBP is not valid
    
        RH = nan;
        Mu     = 0;
        Md     = 0;
        snet   = 0;
        scond  = 0;
        
        return
        
     elseif w>0
        % If upward motion and no cooling, then saturate
        RH = 1;
        
        % Fractional saturation humiidty gradient
        gamma = A - B.*RH;

        % Lapse rate
        Gamma = c.Rv.*T.^2./Lv.*( A-B.*RH + c.g./(c.Rd.*T) );

        % Mass flux ratio
        r = 0;

        % Mass fluxes
        Mu = M;
        Md = 0;
        
        % Net condensation rate
        snet = Mu.*qs.*( gamma - (epsilon+mu.*delta).*(1-RH) );
        
        % Gross condensation rate
        scond = Mu.*qs.*(gamma - epsilon.*(1-RH) );
         
        return
        
    elseif w<0
        % if downward motion and no cooling
        error('Solution for descent with no cooling not available')
    end 

else

    % The cubic equation for the relative humidity
    a(1) = B.*E.* deltaE./deltaT;
    a(2) = -B./deltaT.*( -1 +  E.*deltaE ) + (epsilon + deltaE - B).*E  - ((A+deltaT)./deltaT ).*E.*deltaE;
    a(3) =  (A + deltaT)./deltaT.*( -1 +deltaE.*E) + ( A + B - 2.*(epsilon + deltaE) ).*E + E.*deltaE;
    a(4) = 1 + (epsilon + deltaE - A).*E  - E.*deltaE;

    % Find the relative humidity solutions
    RH = roots(a);

end
    
% Fractional saturation humiidty gradient
gamma = A - B.*RH;

% Lapse rate
Gamma = c.Rv.*T.^2./Lv.*( A-B.*RH + c.g./(c.Rd.*T) );

% Mass flux ratio
r = deltaT.*(1-RH)./( (A-B.*RH).*RH );

% Net condensation rate
snet = 1./r.*( -Q./Lv + M.*deltaE.*(1-RH).*qs );

% Mass fluxes
Mu = snet./(qs.*( gamma - (epsilon+mu.*delta).*(1-RH) ) );
Md = M - Mu;

% Gross condensation rate
scond = (gamma - epsilon.*(1-RH) ).*Mu.*qs;

%% Now figure out the physical solution

% Relative humidity between zero and 1, positive mass flux and negative environmental mass flux
I = find(RH>=0 & RH<=1 & abs(imag(RH))<1e-5 & Mu >= 0 & Md<=0);

if given_RH
    I = 1;
elseif length(I) ~= 1
    error(['There are ' num2str(length(I)) ' solutions for the relative humidity'])
end

RH     = RH(I);
Mu     = Mu(I);
Md     = Md(I);
snet   = snet(I);
scond  = scond(I);
r      = r(I);

Gamma = Gamma(I);
gamma = gamma(I);



end






