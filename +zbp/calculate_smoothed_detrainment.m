function delta_out = calculate_smoothed_detrainment(delta_in,epsilon)

delta_out = delta_in;

% Extend detrainment below lower limit of mass flux
ii = find(~isnan(delta_out),1);
delta_out(1:ii) = delta_out(ii);

% Limit detrainment to 0.1
delta_out(abs(delta_out)>0.1) = 0.1;

ii = find(~isnan(delta_out),1,'last');
delta_out(ii:end) = delta_out(ii);

% Smooth the resultant function with a moving average smoother
for i = 1:20
   delta_out = smooth(delta_out,5);
end