function contents = dirc(directory,varargin)
% gets the contents of a directory (without including . and ..) into a cell
% array of strings
%
% Options: (read as parameter/value pairs, default first)
%
% 'path' : 'relative' | 'full'  -- include the full path to each file
%

   % Defaults
   path_type = 'relative';


   % Read in the options
   for i = 1:2:length(varargin)

      if ~isstr(varargin{i}); error('parameter/value error'); end

      if strcmp(varargin{i},'path'); path_type = varargin{i+1}; end

   end
   

   contents = dir(directory);
   dir_path = {contents.folder};

   contents = contents(~ismember({contents.name},{'.','..'}));
   contents = {contents.name};
   

   if strcmp(path_type,'full') & ~isempty(contents) 
      dir_path = dir_path{1};
      contents = strcat(dir_path,'/',contents);
   end

   
end
