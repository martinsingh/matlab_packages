function [X,Y,varargout] = mesh(x,y,varargin)
%
% Replacement for meshgrid
%
% [X,Y [,Z]] = mutil.mesh(x,y,[,z]);
%
%
% mutil.mesh will make size(X) = [length(x),length(y)]
% meshgrid   will make size(X) = [length(y),length(x)]
%
%

if nargin > 2

  z = varargin{1};
  [X,Y,Z] = meshgrid(x,y,z);

  X = permute(X,[2 1 3]);  
  Y = permute(Y,[2 1 3]);  
  varargout{1} = permute(Z,[2 1 3]);  

else

  [X,Y] = meshgrid(x,y);
  X = permute(X,[2 1]);  
  Y = permute(Y,[2 1]);  

end




