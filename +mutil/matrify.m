function var = matrify(var,Nmat)
%
% Expand an array into more dimensions
%
% Expand an input array to match the size of a higher-dimensional array (Harray)
%
% Usage:
%
%    var_mat = mutil.matrify(var,size(Harray))
%
%
%


%% TODO Deal with equal dimension sizes
% Also deal with signleton dimensions better

% Find the unique dimensions
[U, I] = unique(Nmat, 'first');


% The indices of repeated dimensions
Dim_repeat = 1:length(Nmat);
Dim_repeat(I) = [];




%if length(unique(Nmat)) ~= length(Nmat)
%   error(['At least two dimensions have the same size: ' num2str(Nmat) '. Matrify finds this ambiguous, you will have to expand the matrix manually. Sorry.'])
%end

%% Deal with special cases

% Get the size of the input array
Nvar = size(var);

% If the matrix is already the right size, just return
if isequal(Nvar,Nmat)
    return
end

% If the Harray is actually a scalar, just return
if isequal(Nmat,[1 1])
   return
end

% If the input array is just a scalar, it is very simple
if isequal(Nvar,[1 1])
    var = var.*ones(Nmat);
    return
end

%% The general case

% Find the indices of the common dimensions
[~,Icommon] = ismember(Nvar,Nmat);

% If the input array has non-singleton dimensions that don't match with the H-array give an error
if any(Nvar(Icommon==0) ~= 1)
   I = find(Nvar(Icommon==0) ~= 1);
   error(['Cannot expand the given matrix with dimensions [' num2str(Nvar) '] to matrix with dimensions [' num2str(Nmat) '], problem with dimension(s) [' num2str(I) ']']); 
end

% Remove the singleton dimensions
Icommon = Icommon(Icommon~=0);

% find the indices of the dimensions to be added
Inew = find(~ismember(Nmat,Nvar));


% Expand the matrix
var = squeeze(repmat(var,[ones(1,length(Nvar)) Nmat(Inew)]));

% Put the indices in the right order
var = ipermute(var,[Icommon Inew]);





