function [LW_up,LW_down,SW_up,SW_down,p_half] = calculate_gray_radiation(varargin)
%
% Calculate the vertial fluxes of radiation for an idealised semi-gray
% atmosphere.
%
%
% gray_radiation(p,ps,T,Ts,tau_0,surf_albedo,S_in,solar_tau_0)
% 
% Inputs:
%
%   p = pressure vector (Pa)
%   ps = surface pressure (Pa)
%   T = Temperature vector (K)
%   Ts = surface temperature (K)
%
%   Pressure and temperature vectors must be ordered from TOA to surface
%
%
%   tau_0 = optical thickness at surface
%   surf_albedo = surface albedo
%   S_in = TOA solar radiation (W.m^2)
%   solar_tau_0 = solar radiative optical thickness
%
%
%
%

% Make a plot of the results
plotting = 1;
if nargin>0
   plotting = 0;
end

% Set pressure and temperature vectors to defaults
p = linspace(1000,98000,30);
T = 200+0.0008.*p; T(T<220) = 220;
Ts = T(end)+1;
ps = 100000;

% Set default radiative parameters
surface_albedo =0.38;
tau_0 = 3.5;                  % long-wave optical depth of atmosphere (linear in pressure)
S_in = 0.25.*1360;          % Solar constant W/m^2
solar_tau_0 = 0.22;          % opticlal depth of atmosphere to solar radiation

if nargin >= 1; p = varargin{1}; end
if nargin >= 2; ps = varargin{2}; end
if nargin >= 3; T = varargin{3}; end
if nargin >= 4; Ts = varargin{4}; end

if nargin >= 5; S_in = varargin{5}; end
if nargin >= 6; tau_0 = varargin{6}; end
if nargin >= 7; surface_albedo = varargin{7}; end
if nargin >= 8; solar_tau_0 = varargin{8}; end

% Ensure that the pressure is a vector
p = p(:);

% Additional parameters

% Solar optical depth 
solar_exponent = 2; % Exponent for pressure broadening of solar radiation

% Long-wave optical depth
linear_tau = 0.2;   
wv_exponent = 4;   

% Constants
c = atm.load_constants;




% Vectors are measured top to bottom

% Half levels are flux levels
p_half = [0; (p(2:end)+p(1:end-1))./2; ps];


% Initialize fluxes
LW_down = zeros(1,length(p_half));
LW_up = zeros(1,length(p_half));
SW_down = zeros(1,length(p_half));



% Calculate optical depth
tau = tau_0.*( linear_tau.*(p_half./p_half(end)) + (1.0-linear_tau).*(p_half./p_half(end)).^wv_exponent );


% Calculate solar optical depth
solar_tau = solar_tau_0.*(p_half./p_half(end)).^solar_exponent;

% Transmission of long-wave through layer
dtrans = exp(-(tau(2:end)-tau(1:end-1)  ));

% Stefan Boltzmann
b = c.sigma.*T.^4;

%% Calculate downward fluxes

% Long-wave 
LW_down(1) = 0;
for i = 1:length(T)
   LW_down(i+1) = LW_down(i).*dtrans(i) + b(i).*(1.0 - dtrans(i));
end

% Solar radiation
SW_down = S_in.*exp(-solar_tau)';

%% Calculate upward fluxes

% Ignore the absorptoion of upwelling solar radiation
SW_up = surface_albedo.*SW_down;

% Long-wave (surface has emissivity of unity)
LW_up(end) = c.sigma.*Ts.^4;
for i = length(T):-1:1
   LW_up(i) = LW_up(i+1).*dtrans(i) + b(i).*(1.0 - dtrans(i));
end




if plotting
    
    
    
fig.bfig('a5l')
subplot(131)
plot(T,p./100); set(gca,'ydir','reverse')
xlabel('Temp (K)')
ylabel('pres. (hPa)')
subplot(132)
plot(LW_down,p_half./100,'r'); set(gca,'ydir','reverse')
hold on
plot(SW_down,p_half./100,'b');
plot(SW_up,p_half./100,'b--');
plot(LW_up,p_half./100,'r--')
xlabel('Flux (W/m^2)')

subplot(133)
plot(SW_down-SW_up + LW_down-LW_up,p_half./100,'k-')
xlabel('Net Flux (W/m^2)')
set(gca,'ydir','reverse')

end
















