function [s_c,ds_c,d2s_c] = calculate_critical_entropy(lat,entmax,latmax)

% Load the thermodynamic constants
c = atm.load_constants;

% Load the Earth constants
d = earth.const;

% Temperature difference over the troposphere
dT = 80;

% Trig funcitons
cc = cosd(lat);
ss = sind(lat);
tt = sind(lat)./cosd(lat);

cm = cosd(latmax);
sm = sind(latmax);


% This function plots the critical entropy using the geostropic
% approximation

% Critical Entropy
s_c   = 2.*d.Omega.^2.*d.Re.^2./dT.*( cm.^2./2 - cc.^2./2 + cm.^2.*log(cc./cm) ) +entmax;

% Gradient of critical entropy: 1/R_e*ds_c/dphi
ds_c  = -2.*d.Omega.^2.*d.Re./dT.*tt.*( ss.^2 - sm.^2 );

% Laplacian of critical entropy: 1/(R_e*cos(phi))*d/dphi[ cos(phi)/Re*ds_c/dphi ]
d2s_c = -2.*d.Omega.^2./dT.*(3.*ss.^2 - sm.^2);






end