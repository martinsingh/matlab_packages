% Test of the EBM

%EBM.EBM(heat_method,trans_method,OLR_method,seasonal,ml_depth,beta,MSE0,K,run_length,delta_t,RH);


[lats,day_pentad,T,MSE,E,S,F,OLR,gradMSE,grad2MSE,MSE_crit,gradMSE_crit,grad2MSE_crit,latf,Num_pentad] = EBM.EBM(1,1,1,-10,2,1./1e7,288.*1e7,1e6,2,1800,0.8);

T_1 = T(end,:);
MSE_1 = MSE(end,:);
Flux_1 = F(end,:);
gMSE_1 = gradMSE(end,:);
gMSEc_1 = gradMSE_crit(end,:);

MSEc_1 = MSE_crit(end,:);
g2MSE_1 = grad2MSE(end,:);
g2MSEc_1 = grad2MSE_crit(end,:);

[lats,day_pentad,T,MSE,E,S,F,OLR,gradMSE,grad2MSE,MSE_crit,gradMSE_crit,grad2MSE_crit,latf,Num_pentad] = EBM.EBM(1,3,1,-10,2,1./1e7,288.*1e7,1e6,2,1800,0.8);

T_2 = T(end,:);
MSE_2 = MSE(end,:);
Flux_2 = F(end,:);
gMSE_2 = gradMSE(end,:);
gMSEc_2 = gradMSE_crit(end,:);

MSEc_2 = MSE_crit(end,:);
g2MSE_2 = grad2MSE(end,:);
g2MSEc_2 = grad2MSE_crit(end,:);
