function S = calculate_insolation(day,lat,S0,del_sw)
%
% Calculate the dirunally averaged solar insolation at the TOA
% for given latitudes and a given day
%
seasonal = 1;
if day < 0; seasonal = 0; end

ss = sind(lat);
tt = tand(lat);
cc = cosd(lat);


%% Calculate Solar radiation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 if seasonal ==0
     % Annual mean solar radiation

     % According to Hartmann p 237 a good approximation for the seasonally averaged insolation is:
     S = S0./4.*(1-0.477.*(1.5.*ss.^2 - 0.5));

 else
     % Use 360 day calendar

     % Taken from FMS simulation code, based on Hartmann
     declination = -del_sw*cos( 2.*pi().*(day+10)/(360) );
     h0 = -tt*tan(declination);
     h0(h0>1.0 ) =  1.0;
     h0(h0<-1.0 ) = -1.0;

     h0 = acos(h0);
     S = 1./pi().*S0.*( h0.*ss.*sin(declination) + cc.*cos(declination).*sin(h0) );

 end
