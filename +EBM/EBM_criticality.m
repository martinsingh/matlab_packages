function [lats,day_pentad,T_pentad,varargout] = EBM(varargin)
%
%                     *** ENERGY BALANCE MODEL ***
% SUMMARY: 
%
% This function runs a simple energy balance model (EBM) which calculates
% the latitudinal distribution of temperature given an input solar
% insolation with or without a seasonal cycle. The model has a simple 
% parameterisation for outgoing long-wave radiation and a diffusive
% parameterisation for atmospheric (and oceanic) transport.
%
% Unlike a classic EBM such as that of that of Budyko & Sellers, this model 
% includes options to allow for some of the effects of moist thermodynamics
% in the heat capacity and transport terms. See below for a full
% description of these options. 
%
%
% USAGE: 
%
% The calling format for the model is:
%
%  [lats,day_pentad,T,MSE,E,S,F,OLR,gradMSE,grad2MSE,MSE_crit,gradMSE_crit,grad2MSE_crit,Num_pentad] = EBM.EBM(heat_method,trans_method,OLR_method,seasonal,ml_depth,beta,MSE0,K,run_length,delta_t,RH);
%
% Examples:
%
% Simple EBM with no moist thermodyanmics:
%  [lats,day_pentad,T,MSE,E,S,divF,OLR,gradMSE,grad2MSE,MSE_crit,gradMSE_crit,grad2MSE_crit,latf,Num_pentad] = EBM.EBM(0,0,0,1,2,1,288,1e6,5,1800,0.8)
%
% EBM with heat capacity and transport effects of moisture:
%  [lats,day_pentad,T,MSE,E,S,divF,OLR,gradMSE,grad2MSE,MSE_crit,gradMSE_crit,grad2MSE_crit,latf,Num_pentad] = EBM.EBM(1,1,1,1,2,1./1e7,288.*1e7,1e6,5,1800,0.8)
%
% EBM with heat capacity and transport effects of moisture and tropical dynamics:
%  [lats,day_pentad,T,MSE,E,S,divF,OLR,gradMSE,grad2MSE,MSE_crit,gradMSE_crit,grad2MSE_crit,latf,Num_pentad] = EBM.EBM(1,2,1,1,2,1./1e7,288.*1e7,1e6,5,1800,0.8)
%
% 
% MODEL DESCRIPTION
%
% Model governing equation:
%
%    dE/dt = (1-alpha)*S(phi,t) - OLR - transport
%
%    E is the column integrated energy and S(phi,t) is the solar
%    insolation, either varying seasonally or constant in time, and alpha
%    is the planetary albedo, which is currently fixed to a constant.
%
%    OLR and transport terms are parameterised in a simple way as described
%    below.
%
%    - Definition of E
%
%      E represents the total column energy. There are two choices
%
%      * heat_method = 0
%        Constant heat capacity based on atmospheric heat capacity plus
%        mixed-layer ocean of depth ml_depth
%       
%        E = ( ps/g*c_p + rho_o*c_l*ml_depth ) * T_s
%
%      * heat_method = 1
%        Energy includes moisture content of air
%
%        E = MSE + rho_o*c_l*ml_depth*T_s
%        MSE = column integrated MSE
%
%    - Transport parameterisation
%
%      * trans_method = 0
%        Temperature is diffused as in a classic EBM
%        dE/dt = S(t) - OLR(MSE) - C_atm*K*grad^2(T_s)
%
%      * trans_method = 1
%        MSE is diffused 
%        dE/dt = S(t) - OLR(MSE) - K*grad^2(MSE)
%
%      * trans_method > 1
%        MSE divergence is determined by supercriticality of MSE gradients
%        (defined similar to Emanuel 1995 theory)
%        dE/dt = S(t) - OLR(MSE) - K*( grad^2(MSE) - (K_H-1)*div(grad(MSE) - grad(MSE_crit)) )
% .      K_H = trans_method
%
%    - OLR parameterisation
%
%      OLR is parameterised as a linear function of either temperature or
%      MSE
%
%      * OLR_method = 0
%        OLR = -beta_T*(T-T_0) + OLR0
%
%      * OLR_method = 1
%        OLR = -beta_M*(MSE-MSE_0)  + OLR0
%
%
% The column integrated MSE, if needed, is calculated assuming MSE is
% invarient with height, and given a fixed relative humidity equal to RH. 
%


%% Input parameters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

heat_method = 0;    % Definition of column energy option
trans_method = 0;   % Transport parameterisation option
OLR_method  = 0;    % OLR option

seasonal = 1;		% Include seasonal cycle  in solar radiation: 0 = annual mean, 1 = 360 day year, -N = fixed at day N
beta     = 1;		% OLR sensitivity (units depend on OLR_method)
MSE0     = 288;		% OLR intercept (units depend on OLR_method)
ml_depth = 10;		% Depth of mixed layer ocean (m)
K = 1;              % Diffusion coefficient (m^2/s)


RH = 0.8;           % relative humidity

run_length = 5;     % length of simulation in years
delta_t = 900;		% time step - this must be small enough for stability

%% Inputs %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin >=1; heat_method     		= varargin{1}; end
if nargin >=2; trans_method         = varargin{2}; end
if nargin >=3; OLR_method     		= varargin{3}; end
if nargin >=4; seasonal     		= varargin{4}; end
if nargin >=5; ml_depth     		= varargin{5}; end
if nargin >=6; beta         		= varargin{6}; end
if nargin >=7; MSE0            		= varargin{7}; end
if nargin >=8; K            		= varargin{8}; end
if nargin >=9; run_length      		= varargin{9}; end
if nargin >=10; delta_t      		= varargin{10}; end
if nargin >=11; RH                   = varargin{11}; end




%% Parameters that we don't (usually) vary %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

debug = 0;                  % Do we plot stuff every timestep 

% Solar radiation
S0     = 1360;              % Solar constant        (W/m^2; taken from FMS sims)
del_sw = 0.409;             % Earth tilt axis (radians)
albedo =  0.38.*exp(-0.22); % Surfac albedo for SW  (taken from FMS sims)

% OLR for MSE = MSE0 will balance solar radiation for reasonable albedo
OLR0 = S0./4.*(1-albedo);

ps     = 100000;            % surface pressure

% Set the increase factor if using critical entropy
HCfactor = trans_method-1;       % increase of diffusion coefficient in HC



%% Physical constants %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

c = atm.load_constants;
d = earth.const;
c.Re = d.Re;
c.Omega = d.Omega;

c.ml_depth = ml_depth;
c.ps       = ps;

% To speed up moist calculations
c.coefq    = c.ps.*c.Lv0./c.g;
c.coefT    = (c.ps.*c.cp./c.g + c.rhol.*c.cpl.*c.ml_depth);


%% Derived constants %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Simulation length
run_length_days = run_length*360;
n_steps = ceil(run_length_days.*86400./delta_t);


% Multiply K by heat capacity if diffusing temperature to ensure K is in
% correct units
if trans_method ==0 
    K = K.*(c.cp.*c.ps./c.g);
end


%% Set up grids %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% The model grid is currently hard coded to be 1 deg.
%


latf = linspace(-90,90,181); 			% flux latitudes
lats = (latf(1:end-1)+latf(2:end))./2; 		% scalar latitudes

latsr = lats.*pi./180; 				% scalar ltitudes in radians
latfr = latf.*pi./180; 				% flux ltitudes in radians

% Trig functions
ss = sind(lats);
cc = cosd(lats);
tt = tand(lats);
ccf = cosd(latf);
ssf = sind(latf);

% MSE flux grid
MSE_crit = zeros(size(lats));
gradMSE = zeros(size(latf));
gradMSE_crit = zeros(size(latf));
grad2MSE = zeros(size(lats));
grad2MSE_crit = zeros(size(lats));

% output in pentads
T_pentad = zeros(floor(run_length_days./5),length(lats));	% Surface temperature
MSE_pentad = zeros(floor(run_length_days./5),length(lats));	% Atmospheric MSE
MSEc_pentad = zeros(floor(run_length_days./5),length(lats));	% Atmospheric MSE
gradMSE_pentad = zeros(floor(run_length_days./5),length(latf));	% Atmospheric MSE gradient
gradMSEc_pentad = zeros(floor(run_length_days./5),length(latf));	% Atmospheric MSE critical gradient
grad2MSE_pentad = zeros(floor(run_length_days./5),length(lats));	% Atmospheric MSE critical gradient
grad2MSEc_pentad = zeros(floor(run_length_days./5),length(lats));	% Atmospheric MSE critical gradient
Etot_pentad = zeros(floor(run_length_days./5),length(lats));	% Total column energy
S_pentad = zeros(floor(run_length_days./5),length(lats));	% Solar radiation
divF_pentad = zeros(floor(run_length_days./5),length(lats));	% Energy flux
OLR_pentad = zeros(floor(run_length_days./5),length(lats));	% Long wave radiation
Num_pentad = zeros(floor(run_length_days./5),1);


%% Initial Conditions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% We assume we are running equilibrium runs, so this is simply hard coded
%

% Initial temperature
Tinit = 300-ss.^2.*40; 
T = Tinit;
 
% Calculate initial MSE
MSE = calculate_MSE(T,RH,c);

% Cslculate initial total energy of Column
if heat_method == 0
   Etot = (c.cp.*ps./c.g + c.rhol.*c.cpl.*ml_depth).*T;
else
   Etot = MSE + c.rhol.*c.cpl.*ml_depth.*T;
end
   
% Initial time
day = 0;

%% Stability %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% THese functions help us work out if the model is likely to be stable
%

%dqdT = c.eps./c.ps.*atm.e_sat(c.T0).*c.Lv0./(c.Rv.*c.T0.^2);
%RHS = ml_depth.*c.cpl.*c.rhol./(2.*c.ps./c.g.*(c.cp+c.Lv0.*dqdT));
%LHS = K.*delta_t./(c.Re.*(latsr(2)-latsr(1))).^2;


%% Some text to screen %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('*******************************')
disp(['Running EBM: '])

if heat_method ==0
   disp(['  heat_method = 0:    constant heat capacity'])
else
   disp(['  heat_method = 1:    including moisture in heat capacity'])
end

if OLR_method ==0
   disp(['  OLR_method  = 0:    OLR = -' num2str(beta) '*(T - ' num2str(MSE0) ')' ])
else
   disp(['  OLR_method  = 0:    OLR = -' num2str(beta) '*(MSE - ' num2str(MSE0) ')' ])
end
if trans_method ==0
   disp(['  Diffusing temperature' ])
elseif trans_method ==1
   disp(['  Diffusing MSE' ])
else
   disp(['  Diffusing MSE with criticality' ]) 
end
disp(['Diffusion coef = ' num2str(K) ])
disp(['Running for: ' num2str(run_length) ' years'])
disp(['Timestep: ' num2str(delta_t) ' seconds'])
disp(['total timesteps: ' num2str(n_steps) ' '])
disp(' ')
disp(' ')
disp(' ')
disp('******************************')




%% Begin time loop %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

N_pentad = 0;
k = 1;
maxiter = 0;

for i = 1:n_steps

 
 %% Transport %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
 if trans_method == 0
   MSE_trans = T;
 else
   MSE_trans = MSE;
 end
 
 if OLR_method == 0
   MSE_OLR = T;
 elseif OLR_method==1
   MSE_OLR = MSE;
 end
 
 
 
 % Calculate MSE flux due to midlatitude circulation
 gradMSE(2:end-1) = diff(MSE_trans)./(c.Re.*diff(latsr));
 Flux = -K.*(gradMSE);
 

 % Calculate the flux of MSE owing to the Hadley circulation

 % Calculate the Laplacian of the MSE
 grad2MSE = 1./cc.*diff(ccf.*gradMSE)./(c.Re.*diff(latfr));

 % Calculate the critical entropy assuming zero equator maximum
 [s_c,ds_c,d2s_c] = EBM.calculate_critical_entropy(lats,0,0);
 grad2MSE_crit = c.ps./c.g.*c.T0.*d2s_c;

 % Calculate the location of the maximum in entropy laplacian
 %[~,Ilat0] = min(grad2MSE);
 %supercriticality
 [~,Ilat0] = min(grad2MSE -grad2MSE_crit);
 lat0 = lats(Ilat0);

 % Make sure entropy distribution is supercritical at lat_0
 [s_c,ds_c,d2s_c] = EBM.calculate_critical_entropy(lats,0,lat0);
 grad2MSE_crit = c.ps./c.g.*c.T0.*d2s_c;
 MSE_crit = c.ps./c.g.*c.T0.*s_c + max(MSE);
 [s_c,ds_c,d2s_c] = EBM.calculate_critical_entropy(latf,0,lat0);
 gradMSE_crit = c.ps./c.g.*c.T0.*ds_c;

 if grad2MSE(Ilat0) -grad2MSE_crit(Ilat0) > 0
     
     error('no superciritcality')
 end
 
 % Hadley circulaton exists where entropy gradient is larger than
 % critical entropy gradient
 HC = -sign(latf).*gradMSE > -sign(latf).*gradMSE_crit;
 HC = find(HC,1,'first'):find(HC,1,'last');


 
 if trans_method>1 
    % Add Haldey cell flux
    Flux(HC) = Flux(HC)-HCfactor.*K.*( gradMSE(HC) - gradMSE_crit(HC) ); 
 end

 % Calculate flux weighted by latitude circle area
 Flux = ccf.*Flux;
 
 % Divergence of MSE flux
 divF = 1./cc.*diff(Flux)./(c.Re.*diff(latfr));

 % Flux divergence of heat 
 Ftrans = -divF;


 %% Ooutgoing longwave radiation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 OLR = beta.*(MSE_OLR-MSE0) +OLR0;
 



 %% Calculate Solar radiation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 if seasonal ==0 
     % Annual mean solar radiation
     
     % According to Hartmann p 237 a good approximation for the seasonally averaged insolation is:
     S = S0./4.*(1-0.477.*(1.5.*ss.^2 - 0.5));

 else
     % Use 360 day calendar
     
     if seasonal < 0
         % Fix solar radiation to a particular day within 360 day calendar
         % 350 days gives SH summer Solstice
         day_solar = -seasonal;
     else
         day_solar = day;
     end
     
     % Taken from FMS simulation code, based on Hartmann     
     declination = -del_sw*cos( 2.*pi().*(day_solar+10)/(360) );
     h0 = -tt*tan(declination);
     h0(h0>1.0 ) =  1.0;
     h0(h0<-1.0 ) = -1.0;

     h0 = acos(h0);
     S = 1./pi().*S0.*( h0.*ss.*sin(declination) + cc.*cos(declination).*sin(h0) );
     
   
 end
 
 %% Calculate new timestep values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

 % Calculate time tendency
 dEdt = (S.*(1-albedo) - OLR + Ftrans);
 
 % Use simple explicit scheme - beware the Neuman stability criterion
 Etot = Etot + dEdt.*delta_t;
  
 % Calculate new surface temperature
 if heat_method==0
    T = Etot./(c.cp.*ps./c.g + c.rhol.*c.cpl.*ml_depth);
 else
    [T,iter] = calculate_T(Etot,RH,T,c);
    maxiter = max(maxiter,iter);
 end

 
 
 % Calculate new MSE
 MSE = calculate_MSE(T,RH,c);
 

 % get day number
 day = day + delta_t./86400;

 % If model has crashed exit gracefully
 if sum(isnan(T)) > 0
     %figure
     %plot(lats,T);
     %set(gca,'xlim',[-90 90],'ylim',[220 320]);
     %title(['day ' num2str(day)])
     error('Model crashed. Probably a stability violation. Reduce timestep and try again')
 end
 

 %% outputs %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 % Accumulate output matrices 
 N_pentad = N_pentad+1;
 T_pentad(k,:) = T_pentad(k,:) + T;
 MSE_pentad(k,:) = MSE_pentad(k,:) + MSE;
 MSEc_pentad(k,:) = MSEc_pentad(k,:) + MSE_crit;
 gradMSE_pentad(k,:) = gradMSE_pentad(k,:) + gradMSE;
 gradMSEc_pentad(k,:) = gradMSEc_pentad(k,:) + gradMSE_crit;
 grad2MSE_pentad(k,:) = grad2MSE_pentad(k,:) + grad2MSE;
 grad2MSEc_pentad(k,:) = grad2MSEc_pentad(k,:) + grad2MSE_crit;
 Etot_pentad(k,:) = Etot_pentad(k,:) + Etot;
 S_pentad(k,:) = S_pentad(k,:) + S.*(1-albedo);
 divF_pentad(k,:) = divF_pentad(k,:) + Ftrans;
 OLR_pentad(k,:) = OLR_pentad(k,:) + OLR;

 % If end of pentad save output
 if abs(round(day./5)-day./5) <1e-6

     % If verbose print to screen every five days
     %disp(['day: ' num2str(day) ',   iter: ' num2str(iter)])

     % If end of year print to screen
     if abs(round(day./360)-day./360) <1e-6
        disp(['year: ' num2str(day./360) ', max iter: ' num2str(maxiter)])
     end

     
     T_pentad(k,:) = T_pentad(k,:)./N_pentad;
     MSE_pentad(k,:) = MSE_pentad(k,:)./N_pentad;
     MSEc_pentad(k,:) = MSEc_pentad(k,:)./N_pentad;
     gradMSE_pentad(k,:) = gradMSE_pentad(k,:)./N_pentad;
     gradMSEc_pentad(k,:) = gradMSEc_pentad(k,:)./N_pentad;
     grad2MSE_pentad(k,:) = grad2MSE_pentad(k,:)./N_pentad;
     grad2MSEc_pentad(k,:) = grad2MSEc_pentad(k,:)./N_pentad;
     Etot_pentad(k,:) = Etot_pentad(k,:)./N_pentad;
     OLR_pentad(k,:) = OLR_pentad(k,:)./N_pentad;
     S_pentad(k,:) = S_pentad(k,:)./N_pentad;
     divF_pentad(k,:) = divF_pentad(k,:)./N_pentad;
     day_pentad(k) = day;
     Num_pentad(k) = N_pentad; 
     k = k+1;
     N_pentad = 0;
     
 end
 
 % Plotting for checks
 if debug & mod(i,864000./delta_t) ==0
   clf
   subplot(211)
   plot(lats,T);
   set(gca,'xlim',[0 90],'ylim',[220 320]);
   title(['day ' num2str(i.*delta_t./86400)])
   subplot(212)
   plot(lats,Ftrans)
   set(gca,'xlim',[0 90]);
   pause(0.1)
 end

end % End time loop


%% Send outputs %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargout >=4; varargout{1}= MSE_pentad; end
if nargout >=5; varargout{2}= Etot_pentad; end
if nargout >=6; varargout{3}= S_pentad; end
if nargout >=7; varargout{4}= divF_pentad; end
if nargout >=8; varargout{5}= OLR_pentad; end
if nargout >=9; varargout{6}= gradMSE_pentad; end
if nargout >=10; varargout{7}= grad2MSE_pentad; end
if nargout >=11; varargout{8}= MSEc_pentad; end
if nargout >=12; varargout{9}= gradMSEc_pentad; end
if nargout >=13; varargout{10}= grad2MSEc_pentad; end
if nargout >=14; varargout{11}= latf; end
if nargout >=15; varargout{12}= Num_pentad; end


end



%% Calculate MSE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function MSE = calculate_MSE(T,RH,c)


 % Calculate MSE
 r = c.eps.*RH.*atm.e_sat(T)./(c.ps-RH.*atm.e_sat(T));
 q = r./(1+r);

 % For constant MSE we integrate vertically by multiplying by mass
 MSE = c.ps./c.g.*(c.cp.*T+c.Lv0.*q);


end



%% Calculate total column energy %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Etot = calculate_Etot(T,RH,c)


 MSE = calculate_MSE(T,RH,c);
 Etot = MSE + c.ml_depth.*c.rhol.*c.cpl.*T;
end

function [T_out,k] = calculate_T(Etot,RH,T,c);

  

  % Calculate initial energy and temperature 
  T_in = T;
  E = calculate_Etot(T,RH,c);
  E_in = E;

  % Initialize counter 
  k = 0;
  dT = 1;
  while max(abs(dT)) > 1e-4 & k < 50;
     k= k+1;
  
     % Calculate partial derivative: dq/dT
     e = RH.*atm.e_sat(T);
     q = c.eps.*e./c.ps;
     dqdT = 1./(1-(1-c.eps).*e./c.ps).*q.*c.Lv0./(c.Rv.*T.^2);

     % Linear approximation to T
     dT = (Etot - E)./(c.coefT + c.coefq.*dqdT);
      
     T_out = T + dT;


     % Set values for next iteration
     T = T_out;
     E = calculate_Etot(T,RH,c);

   
     if k > 30
      [maxdT,I] = max(abs(dT));
      disp(['iter = ' num2str(k) ': ' num2str(maxdT)])
      error('too many iterations required')
     end
  end

end




