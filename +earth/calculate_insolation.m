function [S,varargout] = calculate_insolation(lat,time,varargin)
% This function calculates the daily average top-of-atmosphere insolation
% 
% Default is to use Earth-like orbital parameters.
% But one can also specify other parameters
%
% Currently rather slow due to solution of Kepler equation
% Could vectorise this in future
%
% S = calculate_insolation(lat,time [,solar_constant,eccentricity,obliquity,perihelion,period])
%
% lat = latitude                                (degrees; can be vector)
% time = time since Vernal Equinox              (days; MUST BE SCALAR)
%
% solar_constant = mean solar irradiance        (W/m^2)
% eccentrcity = eccentricity of Earth's orbit
% obliquity = obliquity of Earth's orbit        (degrees)
% perihelion = longitude of Perihelion          (degrees)
%
% Earth's orbital parameters taken from
%   Wikipedia: https://en.wikipedia.org/wiki/Solar_irradiance#Irradiation_at_the_top_of_the_atmosphere
%   NASA:      https://pumas.jpl.nasa.gov/files/04_21_97_1.pdf

%% Set defaults

% Solar constant 
S0 = 1367; % W/m^2                      % (Wikipedia)

% Eccentricity of the Earth's orbit 
ecc =  0.016704;                        % (Wikipedia)

% Longitude at Perhelion (relative to NH Spring equinox)
per = 282.895.*pi./180;                 % (Wikipedia)

% Obluiquity 
ob = 23.4398.*pi./180;                  % (Wikipedia)

% Preiod (length of year)
year = 365.2422;                        % (days; NASA) 


%% Read in optional arguments

if nargin > 2; S0   = varargin{1};            end
if nargin > 3; ecc  = varargin{2};            end
if nargin > 4; ob   = varargin{3}.*pi./180;   end
if nargin > 5; per  = varargin{4}.*pi./180;   end
if nargin > 6; year = varargin{5};            end

%% Put inputs in correct units

% latitude in radians
lat = pi./180.*lat;

%% Calculate orbital longitude
% This uses Kepler equations
% See: https://en.wikipedia.org/wiki/Kepler%27s_laws_of_planetary_motion#Position_as_a_function_of_time

% Calculate time since perihelion at NH Spring equinox using Kepler equations
E_eq = 2.*atan(  ( (1-ecc).^0.5.*tan(-per./2) )./(1+ecc).^0.5 );
time_eq = year./(2.*pi).* ( E_eq - ecc.*sin(E_eq) );

% Calculate the requested time since perihelion
time = rem(time + time_eq,year);

% Mean anomaly
M = 2.*pi.*time./year;

% Eccentric anomaly
E = fzero(@(x) M + ecc.*sin(x) - x,M);

% True anomaly (angle from periapsis)
lon = 2.*atan(  ( (1+ecc).^0.5.*tan(E./2) )./(1-ecc).^0.5 );

% Longtiude relative to the NH Spring Equinox
lon = rem(lon + per,2.*pi);

%% Calculate the mean daily solar insolation

% Solar declination
delta = ob.*sin(lon);

% Solar distance relative to mean
R = 1+ecc.*cos(lon - per);

% sunrise/set hour
cosh = - tan(lat).*tan(delta);
cosh(cosh>1) = 1;
cosh(cosh<-1) = -1;

h = acos( cosh  );

% Mean daily insolation
S = S0./pi().* (R.^2).*( h.*sin(lat).*sin(delta) + cos(lat).*cos(delta).*sin(h) );

if nargout > 1; varargout{1} = lon; end

    
