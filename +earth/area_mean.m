function var_mean = area_mean(var,lon,lat,varargin)
%
% Function to calculate the area mean for a variable given on a lat-lon grid
% Lat-lon grid must be staggered with respect to the var points
% area_mean treats nan points as missing.
%
% INPUTS:
%	var(Nlon,Nlat)    = variable on lat-lon grid
%	lon               = lon grid 
%	lat               = lat grid 
%       weight(Nlon,Nlat) = weighting function (optional)
%
%
%  The longitudes and latitudes may be given as
%	1) vectors of length Nlon and Nlat. In this case a staggered grid
%	   is created within this function
%   2) vectors of length Nlon+1 and Nlat+1. In this case, the grid is 
%      is assumed to be staggered relative to the points defining var
%   3) vectors of size 2xNlon 2xNlat. In this case, they are assumed to 
%      give the lower and upper bounds of each gridbox
%
% OUTPUTS:
%	var_mean	  = mean of the variable over lat-lon grid
%

weight = ones(size(var));
if nargin >= 4; weight = varargin{1}; end



% If data is already zonal mean
if length(lon)<=1
    lon = [0 1];
    weight = weight(:)';
end

% Lat-lon grid types
if length(lat) == size(var,2)

   % Grid is given at the same locations as var, need to get edges
   % For the last edge, either extrapolate half a gridpoint,
   % or go to 360 deg lon/90 deg lat

   if abs(lon(end)-lon(1)-360) < mean(diff(lon))
      lon = cat(1,lon(end)-360,lon(:),360+lon(1));
   else
      lon = cat(1,lon(1) - (lon(2)-lon(1)),lon(:),lon(end)+(lon(end)-lon(end-1)));
   end
   lon = (lon(2:end)+lon(1:end-1))/2;

   if ( lat(1) - (lon(2)-lon(1)) ) > -90
       lati = lat(1) - (lon(2)-lon(1));
   else 
       lati = -90;
   end

   if ( lat(end) + (lon(end)-lon(end-1)) ) < 90
       latf = lat(end) + (lon(end)-lon(end-1));
   else 
       latf = 90;
   end

   lat = cat( 1 , lati,lat(:) , latf );
   lat = (lat(2:end)+lat(1:end-1))./2;

elseif length(lat) == size(var,2)+1

   % Grid is already staggered, no worries
   lon = lon(:);
   lat = lat(:);

elseif size(lat,1) == 2 & size(lon,1) == 2

   % We have been given the lon_bnds,lat_bnds
   lon = cat(2,lon(1,1:end-1),lon(2,end));
   lat = cat(2,lat(1,1:end-1),lat(2,end));

   lon = lon(:);
   lat = lat(:);

else

   error('inputs wrong size')

end

lon = repmat(lon(:),size(var(1,:)));
lat = repmat(lat(:)',size(var(:,1)));

dlon = diff(lon,1,1);
dlat = diff(lat,1,2);
coslat = cosd( (lat(:,1:end-1)+lat(:,2:end))./2 );

var_mean = nansum(nansum(coslat.*var.*weight.*dlat.*dlon))./nansum(nansum(coslat.*weight.*dlat.*dlon));



end


