function [Fx,Fy] = grad(s,lon,lat)
% Calculate the gradient of scalar field defined on the Earth (i.e., a Sphere)
%
%
% [Fx,Fy]= grad(s,lon,lat);
%
% Scalar field s defined at points given by lon,lat.
%
% [Fx,Fy] = [ 1/Rcos(lat) ds/dlon , 1/R ds/dlat ]
%
% Assume that derivatives are zero at pole
%

%% Load the earth constants
c = earth.const;

%% Make axis matrices
[lon_mat,lat_mat] = mutil.mesh(lon,lat);


%% Define axes at interface points
loni = cat(1,lon_mat(end,:)-360,lon_mat,360+lon_mat(1,:));
loni = (loni(2:end,:)+loni(1:end-1,:))./2;
dlon = diff(loni,1,1).*pi()./180;

lati = cat( 2 , -90.*ones(size(lat_mat(:,1))),lat_mat , 90.*ones(size(lat_mat(:,1))) );
lati = cat(1,lati(:,2:end)+lati(:,1:end-1))./2;
dlat = diff(lati,1,2).*pi./180;

% Define F on the interface points
sxi = cat(1,s(end,:),s,s(1,:));
sxi = (sxi(2:end,:)+sxi(1:end-1,:))./2;

syi = cat(2,s(:,1),s,s(:,end));
syi = (syi(:,2:end)+syi(:,1:end-1))./2;

Fx = 1./(c.Re.*cosd(lat_mat)) .*  diff(sxi,1,1)./dlon; 
Fy = 1./c.Re .* diff(syi,1,2)./dlat ;

