function [l_frac,lon,lat] = ls_mask(varargin)
%
% Gives a land mask at 0.1 deg resolution
% Possibly add more resolutions in the future

% [l_frac,lon,lat] = ls_mask(resolution)
%
% l_frac = fraction of area covered by land
% lon = longitude (deg)
% lat = latitude (deg)
% 
% resolution = 	'0.1deg' (default)
%
% 
resolution = '0.1deg';

if nargin >=1; resolution = varargin{1}; end

this_dir = fileparts(mfilename('fullpath'));

switch resolution

  case '0.1deg'

    load([this_dir '/ls_mask_0.1deg.mat'],'l_frac','lat','lon')


  otherwise
    error('Cannot find resolution required')

end

