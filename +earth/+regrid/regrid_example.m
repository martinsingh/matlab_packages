%% Calculate a regridding procedure

%
% Test script for conservative remapping
%


%% Parameters

% Initial grid spacing
dloni = 1;
dlati = 1;

% Final grid spacing
dlonf = 2.5;
dlatf = 2.5;


%% Set the boundary values
lon_bnds = [0:dloni:360-dloni; dloni:dloni:360];
lat_bnds = [-90:dlati:90-dlati; -90+dlati:dlati:90];


lonn_bnds = [0:dlonf:360-dlonf; dlonf:dlonf:360];
latn_bnds = [-90:dlatf:90-dlatf; -90+dlatf:dlatf:90];


%% Calculate the grid centres
lat = mean(lat_bnds);
lon = mean(lon_bnds);

latn = mean(latn_bnds);
lonn = mean(lonn_bnds);


%% Some fake precipitation field
[lat_mat,lon_mat] = meshgrid(lat,lon);
P = rand(length(lon),length(lat)).^12.*(0.95.*cosd(lat_mat).^2+0.05);


%% Do the regridding

% Calculate the weights for the regridding
[Wlon,Wlat] = earth.regrid.calculate_weights(lon_bnds,lat_bnds,lonn_bnds,latn_bnds);


% Regrid the precipitation
tic
P_regridded = earth.regrid.conservative_interpolation(Wlon,Wlat,P);
toc

% Compare this to simple interpolation
P_interp = interp2(lon,lat,P',lonn',latn)';


%% Calculate the area of each gridbox (as fraction of globe)

% Calculate the grid sizes
dlat = lat_bnds(2,:) - lat_bnds(1,:);
dlon = lon_bnds(2,:) - lon_bnds(1,:);

dlatn = latn_bnds(2,:) - latn_bnds(1,:);
dlonn = lonn_bnds(2,:) - lonn_bnds(1,:);


% Calculate some grid matrices
[lat_mat,lon_mat] = meshgrid(lat,lon);
[dlat_mat,dlon_mat] = meshgrid(dlat,dlon);

[latn_mat,lonn_mat] = meshgrid(latn,lonn);
[dlatn_mat,dlonn_mat] = meshgrid(dlatn,dlonn);


Area = cosd(lat_mat).*dlat_mat.*dlon_mat.*pi./(4*180.^2);
Arean = cosd(latn_mat).*dlatn_mat.*dlonn_mat.*pi./(4*180.^2);

%% Calculate the mean precipitation over a box
lons = [160 180];
lats = [0 10];

I = find(lon<lons(2) & lon>lons(1));
J = find(lat<lats(2) & lat>lats(1));
Pmean = sum(sum(Area(I,J).*P(I,J)))./sum(sum(Area(I,J)));

I = find(lonn<lons(2) & lonn>lons(1));
J = find(latn<lats(2) & latn>lats(1));
Pmean_regridded = sum(sum(Arean(I,J).*P_regridded(I,J)))./sum(sum(Arean(I,J)));
Pmean_interpolated = sum(sum(Arean(I,J).*P_interp(I,J)))./sum(sum(Arean(I,J)));



%% Now plot the regridding
fig.bfig(15,20)
fig.set_default('paper')

set(gcf,'defaultaxesfontsize',9)

clf
subplot(311)
pcolor(lon,lat,P'); shading flat; colorbar
hold on
plot([lons(1) lons(1)],[lats(1) lats(2)],'w')
plot([lons(2) lons(2)],[lats(1) lats(2)],'w')
plot([lons(1) lons(2)],[lats(1) lats(1)],'w')
plot([lons(1) lons(2)],[lats(2) lats(2)],'w')
set(gca,'clim',[0 0.5])
ylabel('latitude (deg)')

title(['Original: box mean = ' num2str(Pmean)])


subplot(312)
pcolor(lonn,latn,P_regridded'); shading flat; colorbar
hold on
plot([lons(1) lons(1)],[lats(1) lats(2)],'w')
plot([lons(2) lons(2)],[lats(1) lats(2)],'w')
plot([lons(1) lons(2)],[lats(1) lats(1)],'w')
plot([lons(1) lons(2)],[lats(2) lats(2)],'w')
set(gca,'clim',[0 0.5])
ylabel('latitude (deg)')

title(['Conservative: box mean = ' num2str(Pmean_regridded)])

subplot(313)
pcolor(lonn,latn,P_interp'); shading flat; colorbar
hold on
plot([lons(1) lons(1)],[lats(1) lats(2)],'w')
plot([lons(2) lons(2)],[lats(1) lats(2)],'w')
plot([lons(1) lons(2)],[lats(1) lats(1)],'w')
plot([lons(1) lons(2)],[lats(2) lats(2)],'w')
set(gca,'clim',[0 0.5])
ylabel('latitude (deg)')

title(['Interpolated: box mean = ' num2str(Pmean_interpolated)])

xlabel('longitude')



%% Now do a real case of mean precipitation

% Get the coasts
[lonc,latc] = earth.coasts;

% Set the file as CMAP mean precipitation
path = mfilename('fullpath');
[path,~,~] = fileparts(path);
file_in = [path '/CMAP_precip_climatology.nc'];

% Read the precipitation
precip = ncread(file_in,'precip');

% Read the axes
lat = ncread(file_in,'lat');
lon = ncread(file_in,'lon');

% Set the missing values to nan
precip(precip<-100) = nan;

% Calculate the annual mean
precip = nanmean(precip,3);

% Flip the latitude around
precip = flip(precip,2);
lat = flip(lat);

% Calculate latitude boundaries
lat_bnds = [-90; (lat(1:end-1)+lat(2:end))./2 ];
tmp = [(lat(1:end-1)+lat(2:end))./2; 90 ];
lat_bnds = [lat_bnds tmp]';

% Calculate longitude boundaries
lon_bnds = [(lon(end)-360+lon(1))./2; (lon(1:end-1)+lon(2:end))./2];
tmp = [(lon(1:end-1)+lon(2:end))./2; (lon(end)+lon(1)+360)./2];
lon_bnds = [lon_bnds tmp]';

% Final grid spacing
dlonf = 10;
dlatf = 5;

lonn_bnds = [0:dlonf:360-dlonf; dlonf:dlonf:360];
latn_bnds = [-90:dlatf:90-dlatf; -90+dlatf:dlatf:90];

latn = mean(latn_bnds);
lonn = mean(lonn_bnds);


% Calculate the weights for the regridding
[Wlon,Wlat] = earth.regrid.calculate_weights(lon_bnds,lat_bnds,lonn_bnds,latn_bnds);


% Regrid the precipitation
tic
[precip_regridded,F] = earth.regrid.conservative_interpolation(Wlon,Wlat,precip);
toc

fig.bfig(15,20)
fig.set_default('paper')

set(gcf,'defaultaxesfontsize',9)

subplot(311)
pcolor(lon,lat,precip'); shading flat; set(gca,'clim',[0 10]);
hold on
plot(lonc,latc,'k')
plot(lonc+360,latc,'k')
c = colorbar;
xlabel('longitude')
ylabel('latitude')
ylabel(c,'mm/day')
set(gca,'xlim',[0 360])
colormap(1-copper);

subplot(312)
pcolor(lonn,latn,precip_regridded'); shading flat; set(gca,'clim',[0 10]);
hold on
plot(lonc,latc,'k')
plot(lonc+360,latc,'k')
c = colorbar
xlabel('longitude')
ylabel('latitude')
ylabel(c,'mm/day')
set(gca,'xlim',[0 360])
colormap(1-copper);

subplot(313)
pcolor(lonn,latn,F'); shading flat; set(gca,'clim',[0 1]);
hold on
plot(lonc,latc,'k')
plot(lonc+360,latc,'k')
colorbar
xlabel('longitude')
ylabel('latitude')
ylabel(c,'mm/day')
set(gca,'xlim',[0 360])

