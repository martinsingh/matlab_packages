function [Pn,varargout] = conservative_interpolation(Wlon,Wlat,P)
%
% Conservative interpolaton
%
% Interpolates a 2D field on a lat-lon grid to another lat-lon grid 
% using a first-order conservative remapping. This means that the 
% integral of the field over the surface of the Earth is conserved.
%
% [Pn,f] = conservative_interpolation(Wlat,Wlon,P)
%
% P = original field as a function of latitude and longitude
%
% Wlat = weight matrix calculated using function "calculate_weights.m"
% Wlon = weight matrix calculated using function "calculate_weights.m"
%
% Pn = field on the new grid
% f = fraction of each gridbox that contained data on the original grid
%
%

PP = zeros(size(Wlon,1),size(Wlat,2));
Pn = zeros(size(Wlon,1),size(Wlat,1));


%% Calculate the regridding using the previously calculated weights

for i = 1:size(Wlon,1)
   PP(i,:) = nansum(P.*Wlon(i,:)')./sum(~isnan(P).*Wlon(i,:)');
end

for i = 1:size(Wlat,1)
   Pn(:,i) = nansum(PP.*Wlat(i,:),2)./sum(~isnan(PP).*Wlat(i,:),2);
end

I = zeros(size(Wlon,1),size(Wlat,2));
frac = zeros(size(Wlon,1),size(Wlat,1));

if nargout > 1
    
    % Calculate the fraction of each gridbox for which data exists

   for i = 1:size(Wlon,1)
      I(i,:) = sum(~isnan(P).*Wlon(i,:)');
   end

   for i = 1:size(Wlat,1)
      frac(:,i) = sum(~isnan(PP).*Wlat(i,:),2);
   end

   varargout{1} = frac;
   
end











