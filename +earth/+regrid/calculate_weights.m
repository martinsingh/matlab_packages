function [Wlon,Wlat] = calculate_weights(lon_bnds,lat_bnds,lonn_bnds,latn_bnds)
%
% Calculate the weight matrices to do conservative interpolaton
%
% This function may be used with conservative_interpolation.m 
% to perform first-order conservative remapping of 2D fields
%
% [Wlon,Wlat] = calculate_weights(lat_bnds,lon_bnds,latn_bnds,lonn_bnds);
%
% lat_bnds = latitude edges of old grid (2xNlat)
% lon_bnds = longitude edges of old grid (2xNlon)
% latn_bnds = latitude edges of new grid (2xNlatn)
% lonn_bnds = longitude edges of new grid (2xNlonn)
%
% The variables above contain the lower edge of a gridbox in the first row
% and the higher edge of a gridbox in the second row. This is consistent
% with similar variables in CMIP5 model files etc.
%
%  

%% Calculate the properties for the old grid

% Grid centres
lon = mean(lon_bnds);
lat = mean(lat_bnds);

% Grid size
dlon = (lon_bnds(2,:) - lon_bnds(1,:) );
dlat = (lat_bnds(2,:) - lat_bnds(1,:) );

% Area of gridboxes as fraction of the total Earth surface area
Area = cosd(lat).*dlat.*pi./(4*180.^2);


%% Calculate the properties of the new grid

lonn = mean(lonn_bnds);
latn = mean(latn_bnds);



dlonn = (lonn_bnds(2,:) - lonn_bnds(1,:) );
dlatn = (latn_bnds(2,:) - latn_bnds(1,:) );

%% Calculate the latitude weights

Wlat = zeros(length(latn),length(lat));
for i = 1:size(latn_bnds,2)
    
    lmin = latn_bnds(1,i);
    lmax = latn_bnds(2,i);
    dl = lat_bnds(2,:) - lat_bnds(1,:);
    
    % Find gridboxes that are at least partially overlapping with interval  
    I = Area.*(lat_bnds(2,:) >= lmin & lat_bnds(1,:) <= lmax);
    
    % Get the fraction of the partial gridboxes
    J = I.*(lat_bnds(2,:)-lmin)./dl;
    K = I.*(lmax - lat_bnds(1,:))./dl;
    
    II = min([I;J;K]);
    
    % Set the weights
    Wlat(i,:) = II./sum(II);


end


%% Calculate the longitude weights

Wlon = zeros(length(lonn),length(lon));
for i = 1:size(lonn_bnds,2)
    
    lmin = lonn_bnds(1,i);
    lmax = lonn_bnds(2,i);
    dl = lon_bnds(2,:) - lon_bnds(1,:);
    
    % Find gridboxes that are at least partially overlapping with interval  
    I = (lon_bnds(2,:) >= lmin & lon_bnds(1,:) <= lmax);
    
    % Check for the case in which the lower bound is a negative longitude
    J = (lon_bnds(2,:)+360 >= lmin & lon_bnds(1,:)+360 <= lmax);
    
    I = I|J;
    
    % Get the fraction of the partial gridboxes
    J = I.*(lon_bnds(2,:)-lmin)./dl;
    K = I.*(lmax - lon_bnds(1,:))./dl;
    L = I.*(lmax - lon_bnds(1,:)+360)./dl;
    II = min([I;J;K;L]);
    
    % Set the weights
    Wlon(i,:) = II./sum(II);


end


end
