% Run some simple tests to check things are okay

%% grad/div operator

lon = 1:359;
lat = -89:89;

%% Make axis matrices
lon_mat = repmat(lon(:),[1 length(lat)]);
lat_mat = repmat(lat(:)',[length(lon) 1]);

s = lat_mat.^2;
u = 0.2.*(3+sind(lon_mat)).*cosd(lat_mat);
v = exp(-(lon_mat-80).^2./(2.*30.^2) - (lat_mat+10).^2./(2.*20.^2) );

[dsx,dsy] = earth.grad(s,lon,lat);
[dtx,dty] = earth.grad(u,lon,lat);
[dux,duy] = earth.grad(v,lon,lat);

divuv = earth.div(u,v,lon,lat);
curluv = earth.curl(u,v,lon,lat);


if 0
  fig.bfig(19,10)

  contourf(lon,lat,divuv',22,'linestyle','none')
  hold on
  contour(lon,lat,curluv',[4 8 12 16 20 24].*1e-8,'g')
  contour(lon,lat,curluv',[24 20 16 12 8 4].*(-1e-8),'m')
  contour(lon,lat,curluv',[0 0],'k')
  
  quiver(lon(1:8:end),lat(1:8:end),u(1:8:end,1:8:end)',v(1:8:end,1:8:end)','k')
  colorbar
  set(gca,'clim',[-0.5 0.5].*1e-6);
  fig.red2blue
  ylabel('latitude')
  xlabel('longitude')
end

%% Area mean

smean = earth.area_mean(s,lon,lat);
umean = earth.area_mean(u,lon,lat);
vmean = earth.area_mean(v,lon,lat);


lon2 = [10 20 30];
lat2 = [-20 -10 0 10 20];
P = rand(length(lon2),length(lat2)).^4;

Pmean = earth.area_mean(P,lon2,lat2);








