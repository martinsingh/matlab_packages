function [min_distances,N] = find_connections(A,lon,lat)
%
% Find the minimum spherical distance (in degrees) between connected 
% regions in a binary matrix
%
% min_distance = find_connections(A,lon,lat) 
%
% A is a binary matrix defined on a longitude-latitude grid
% min_distance is a matrix with the i,j element corresponding to the
% shortest distance between the ith and jth blob.

%% Check the inputs
S = size(A);
if length(S) ~=2
    error('input A must be matrix')
end
if S(1) ~= length(lon)
    error('longitude not correct size')
end
if S(2) ~= length(lat)
    error('latitude not correct size')
end


%% Make an longitude and latitude matrix
[latm,lonm] = meshgrid(lat,lon);

%% Calculate the connected components
L = labelmatrix(bwconncomp(A));

%% Fix the edge cases to conform to the doubly periodic geometry
I = L(1,:) > 0 & L(end,:) > 0;
L(1,I) = min(L(1,I),L(end,I));
L(end,I) = min(L(1,I),L(end,I));

% Find the unique regions
labels = unique(L);

% Remove the first element which is the zeros
labels = labels(2:end);

% Number of regions
N = length(labels);
disp(['calculating ' num2str(N) ' regions'])

% Initialise minimum distance matrix
min_distances = zeros(N);

%% Loop over all the connected regions
for i = 1:N-1

    % Find the latitudes and longitudes of blob i
    [I,J] = ind2sub(S,find(L==labels(i)));
    Ni = length(I);
    loni = lon(I)';
    lati = lat(J)';
    
    % Create 3D matrices
    % Each 2D slice of I is the longitude index of one of its non-zero entries
    loni = repmat(permute(loni,[2 3 1]),[S(1) S(2) 1]);
    
    % Each 2D slice of J is the latitude index of one of its non-zero entries
    lati = repmat(permute(lati,[2 3 1]),[S(1) S(2) 1]);
    
    if Ni > size(lonm,3)
       lonm = repmat(lonm(:,:,1),[1 1 Ni]);
       latm = repmat(latm(:,:,1),[1 1 Ni]);
    end
    
    % Calculate the distance transform
    B_dist = distance(latm(:,:,1:Ni),lonm(:,:,1:Ni),lati,loni);
    B_dist = min(B_dist,[],3);
    
    %% Find the lowest value of B_dist occuring at a non-zero value of A
    for j = i+1:N
       min_distances(i,j) = min(B_dist(L==labels(j)));
       min_distances(j,i) = min_distances(i,j);
    end
    
    
end


end





