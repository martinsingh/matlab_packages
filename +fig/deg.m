function cell_out = deg(cell_in)
% Adds degree symbol to a cell array of strings
% For use in axis labels

if isnumeric(cell_in)
   cell_out = cellstr(string(cell_in)); 
else
   cell_out = cell_in;
end
for i = 1:length(cell_out)

   if ~isempty(cell_out{i})
      cell_out{i} = [cell_out{i} char(176)];
   end
end

