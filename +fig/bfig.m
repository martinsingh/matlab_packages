function h = bfig(W,H);
%
% Function to create a figure with specified width and height
% Includes shortcuts for a4 and a5 sizes.
%
% h = bfig(W,H)
%
% Creates figure with handle h with width W and height H in centimeters
% Paper is also set so that figure can be printed to pdf 
% and no white space exists
%
% Also accepts syntax
% h = bfig('x')
% x = {'a4l' 'a4p' 'a5l' 'a5p'}
%
%




   h = figure;
   set(gcf,'units','centimeters','paperunits','centimeters');

if ischar(W)
  if strcmp(lower(W),'a5l'),
    W=21;
    H=14.8;
    set(gcf,'papertype','A5');
    set(gcf,'paperorientation', 'landscape');
  elseif strcmp(lower(W),'a4p')
    W=21;
    H=29.7;
    set(gcf,'papertype','A4');
    set(gcf,'paperorientation', 'portrait');
  elseif strcmp(lower(W),'a4l')
    W=29.7;
    H=21;
    set(gcf,'papertype','A4');
    set(gcf,'paperorientation', 'landscape');
  elseif strcmp(lower(W),'a5p'),
    H=21;
    W=14.8;
    set(gcf,'papertype','A5');
    set(gcf,'paperorientation', 'portrait');
  else
    error('unkown paper size')
  end
else
end
  set(gcf,'paperposition',[0 0 W H],'papersize',[W H]);  


set(gcf,'position',[0 0 W H]);
   
   
   

    


end
