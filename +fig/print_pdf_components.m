function print_pdf_components(filename,varargin)
% This function splits a figure into objects that have filled colors, which
% have the white line problem that has plagues matlab for a decade, and
% line objects. These may be printed as bitmap and vector, repectively, and
% then combined using inkscape so that you can still have scalable text on
% figures that have filled contours etc
%
%
%

%% Inputs
H_fig = gcf;
if nargin >= 2; H_fig = varargin{1}; end;


%% Print using standard method the original figure
print(H_fig,'-dpdf',[filename,'.pdf'])

%% Copy the entire figure twice
H_figb = copyobj(H_fig,groot);
H_figv = copyobj(H_fig,groot);

%% Make bitmap

% Get the handles to all the objects
H = findobj(H_figb);

% get the handles to the filled contour objects
H_filled = findobj(H_figb,'fill','on');

% getthe handles to any surfaces
H_surf = findobj(H_figb,'type','surface');

% get the handles to the colorbars
H_colorbar = findobj(H_figb,'type','colorbar');

% Get the handle to the axes
H_axes = findobj(H_figb,'type','axes');

% Find the rest of the objects: these will be deleted
I_vector = ~ismember(H,[H_filled;H_figb;H_axes;H_colorbar]);
H_vector = H(I_vector);

% delete the objects that will be vectors
delete(H_vector)

% Make the axes invisible
set(H_axes,'visible','off')

% Get rid of colorbar axes (as well as possibile)
set(H_colorbar,'box','off','ticks',[],'linewidth',[0.1],'color','none')

for i = 1:length(H_colorbar)
   ylabel(H_colorbar(i),'')
end

%% Print bitmap
print(H_figb,'-opengl','-r900','-dpdf',[filename,'_bitmap.pdf'])

%% Make the vectorized figure

% get the handles to the filled contour objects
H_filled = findobj(H_figv,'fill','on');

% getthe handles to any surfaces
H_surf = findobj(H_figv,'type','surface');

% get the handles to the colorbars
H_colorbar = findobj(H_figv,'type','colorbar');

delete([H_filled;H_surf])

% get the handles to the axes
H_axes = findobj(H_figv,'type','axes');
set(H_axes,'color','none')

set(H_figv,'color','none')

%% Print bitmap
print(H_figv,'-painters','-dpdf',[filename,'_vector.pdf'])


end

