function varargout = red2blue(varargin)
%
% Function to make a red-blue anomaly colormap 
% for gca (default) or axis given as input argument
%
%
% cbar = red2blue([axis,reverse,N])
%
%
% axis    = axis to apply the colormap to 	(default gca)
% reverse = make it blue-to-red  		(default 0)
% N       = Number of colors in map 		(default 64)
%


reverse = 0;
N =64;
if isempty(varargin);     axis = gca;            end
if length(varargin) >= 1; axis = varargin{1};    end
if length(varargin) >= 2; reverse = varargin{2}; end
if length(varargin) >= 3; N = varargin{3};       end
if length(varargin) >= 4; error('Too many arguments.'); end
    
limits = get(axis,'clim');    

if limits(1)>0
    % No negative values; colobar will be red-to-white
    disp('GT')
    limits(1) = 0;
    set(axis,'clim',limits)
    
elseif limits(2)<0
    % No positive values; colobar will be white-to-blue
    disp('LT')
    limits(2) = 0;
    set(axis,'clim',limits)
end
    

zeropt = (N+1)* (-limits(1))/(limits(2)-limits(1));

red = zeros(N,1);
green = zeros(N,1);
blue = zeros(N,1);

red(1:floor(zeropt)) = linspace(0.5,1,floor(zeropt));
blue(1:floor(zeropt)) = linspace(0,1,floor(zeropt));
green(1:floor(zeropt)) = linspace(0,1,floor(zeropt));


red(floor(zeropt)+1:end) = linspace(1,0,N-floor(zeropt));
green(floor(zeropt)+1:end) = linspace(1,0,N-floor(zeropt));
blue(floor(zeropt)+1:end) = linspace(1,0.5,N-floor(zeropt));

the_colors = [blue,green,red];

if reverse
    the_colors = the_colors(:,end:-1:1);
end

colormap(axis,the_colors)

if nargout ==1
   varargout{1} = the_colors;
end
end
