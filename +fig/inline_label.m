function ll = inline_label(pp,tt,varargin)
% Create a label for a line created with "plot"
% 
% ll = inline_label(pp,tt,xloc,yloc)
%
% pp = plot handle
% tt = text
% xloc = approximate x location of label
% yloc = approxiamte y location of label

xloc = [];
yloc = [];
params = {};

if nargin > 2; xloc = varargin{1}; end
if nargin > 3; yloc = varargin{2}; end
if nargin > 4; params = {varargin{3:end}}; end;
x = pp.XData;
y = pp.YData;


%% Get the location of the label
sw =  [isempty(xloc) isempty(yloc)];
    
    
    if isequal(sw,[0 0])
        
        [~,I] = min((x - xloc).^2 + (y - yloc).^2);
        
    elseif isequal(sw,[1 0])
        
        [~,I] = min((y - yloc).^2);
        
    elseif isequal(sw,[0 1])
        
        [~,I] = min((x - xloc).^2);

    elseif isequal(sw,[1 1])
    
        xloc = nanmean(x);
        yloc = nanmean(y);
        [~,I] = min((x - xloc).^2 + (y - yloc).^2);
        
    else
        
        error('This should never happen. You have broken me')
        
    end

xloc = x(I);
yloc = y(I);
        
Ip1 = min(I+1,length(x));
Im1 = max(I-1,1);

%% Rotation of the label
sw =  [isnan(Ip1) isnan(Im1)];
    
    if isequal(sw,[1 0])
        Ip1 = I;
    elseif isequal(sw,[0 1])
        Im1 = I;
    elseif isequal(sw,[1 1])
        Ip1 = I;
        Im1 = I;
    end
        
% Get the rise and run
ax = pp.Parent;
xl = get(ax,'xlim');
yl = get(ax,'ylim');
posa = get(ax,'position');

fig = ax.Parent;
posf = get(fig,'position');
dx = (x(Ip1)-x(Im1))./(xl(2)-xl(1)).*posa(3).*posf(3);
dy = (y(Ip1)-y(Im1))./(yl(2)-yl(1)).*posa(4).*posf(4);
    
phi = atand(dy./dx);

if isnan(phi); phi = 0; end


ll = text(double(xloc),double(yloc),tt,'horizontalalignment','center','verticalalignment','middle','rotation',double(phi),'backgroundcolor','white','margin',0.5,params{:});





