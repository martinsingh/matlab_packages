function theResult = ncload(theNetCDFFile, varargin)

% ncload -- Load NetCDF variables.
%  ncload('theNetCDFFile', 'var1', 'var2', ...) loads the
%   given variables of 'theNetCDFFile' into the Matlab
%   workspace of the "caller" of this routine.  If no names
%   are given, all variables are loaded.  The names of the
%   loaded variables are returned or assigned to "ans".
%   No attributes are loaded.
 
% Copyright (C) 1997 Dr. Charles R. Denham, ZYDECO.
%  All Rights Reserved.
%   Disclosure without explicit written consent from the
%    copyright owner does not constitute publication.
 
% Version of 18-Aug-1997 10:13:57.

%
% Rewritten using native netcdf functions my Martin Singh.
% May still be a bit buggy

if nargin < 1, help('ncutil.ncload'), return, end

result = [];
if nargout > 0, theResult = result; end

f = netcdf.open(theNetCDFFile, 'nowrite');
if isempty(f), return, end

if isempty(varargin)

    varids = netcdf.inqVarIDs(f);
    
    for i = 1:length(varids)
       varname = netcdf.inqVar(f,varids(i));
       var_data = netcdf.getVar(f,varids(i));
       assignin('caller', varname, double(var_data))
    end
else

    for i = 1:length(varargin)
        if ~ischar(varargin{i}), varargin{i} = inputname(i+1); end
        varid = netcdf.inqVarID(f,varargin{i});
        var_data = netcdf.getVar(f,varid);
        assignin('caller', varargin{i}, double(var_data))
    end
end

result = varargin;

netcdf.close(f)

if nargout > 0
   theResult = result;
end
