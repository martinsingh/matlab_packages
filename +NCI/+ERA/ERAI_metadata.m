function const = ERAI_metadata


const.data_loc = '/g/data/ub4/erai/netcdf/';

const.freq = {'3hr' '6hr'};
const.realms = {'atmos' 'land' 'ocean' 'seaIce'};
const.data_types = {'sfc' 'pl' 'pt' 'pv' 'ml'};

% Land
const.land.sfc.path = 'land/oper_an_sfc/v01/';
const.land.sfc.vars = {};

% Ocean
const.ocean.sfc.path = 'ocean/oper_an_sfc/v01/';
const.ocean.sfc.vars = {};

% Sea Ice
const.seaIce.sfc.path = 'seaIce/oper_an_sfc/v01/';
const.seaIce.sfc.vars = {};

% Atmosphere
const.atmos.sfc.path = 'atmos/oper_an_sfc/v01/';
const.atmos.sfc.vars = {'tas' 'ps' 'uas' 'vas'};

const.atmos.pl.path = 'atmos/oper_an_pl/v01/';
const.atmos.pl.vars = {'hus' 'ta' 'ua' 'va' 'wap' 'z' 'vo' 'd' 'cli' 'clw' 'clt'};

const.atmos.pt.path = 'atmos/oper_an_pt/v01/';
const.atmos.pt.vars = {};

const.atmos.pv.path = 'atmos/oper_an_pv/v01/';
const.atmos.pv.vars = {};

const.atmos.ml.path = 'atmos/oper_an_ml/v01/';
const.atmos.ml.vars = {'hus' 'ta' 'ua' 'va' 'wap' 'z' 'vo' 'd' 'cli' 'clw' 'clt'};


this_dir = fileparts(mfilename('fullpath'));
const.lev_file = [this_dir '/ERA-Interim_coordvars.nc'];
const.fx_file = [const.data_loc 'fx/ei_invariant_0-360.nc'];

const.years = [197901 201908];
