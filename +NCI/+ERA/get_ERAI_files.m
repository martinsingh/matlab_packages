function files = get_ERAI_files(variable,varargin)
%
% Function to list the data files availible for a given variable in ERAI
%
% [files,times] = get_ERAI_files(variable,realm,data_type,freq)
%
%
%
%


c = NCI.ERA.ERAI_metadata;

% Defaults
realm = 'atmos';
data_type = 'ml';
freq = '6hr';

if nargin >=2;  realm = varargin{1}; end
if nargin >=3;  data_type = varargin{2}; end
if nargin >=3;  freq = varargin{3}; end


dir_in = [c.data_loc '/' freq '/' c.(realm).(data_type).path '/' variable '/'];
info = dir([dir_in '/' variable '*.nc' ]); 

files = {info(:).name};

times = zeros(length(files),2);
for i = 1:length(files)

   ff = [dir_in '/' files{i}];
   files{i} = ff; 
   times(i,1) = datenum(ff(end-10:end-3),'yyyymmdd')
   times(i,2) = datenum(ff(end-19:end-12),'yyyymmdd')


end





