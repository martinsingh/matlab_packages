function const = ERA_metadata


const.data_loc = ['/g/data1/rr7/'];
const.mat_loc = '../../mat_data/Reanal/';
const.matdata_path = [getenv('DATA1') '/mat_data/Reanal/'];

const.models = {'ERA_INT' 'ERA40'};

const.variables = {'zg' 'ua' 'va' 'ta' 'hus' 'huss' 'tas' 'pr' 'ps' 'psl' 'uas' 'vas'};
const.years = [1979 2011];
