function  axes = get_ERAI_axes

c = NCI.ERA.ERAI_metadata;


 f = netcdf.open(c.fx_file,'nowrite');

 %% Latitude and longitude
 varid = netcdf.inqVarID(f,'latitude');
 axes.lat    = netcdf.getVar(f,varid);

 varid = netcdf.inqVarID(f,'longitude');
 axes.lon    = netcdf.getVar(f,varid);

 netcdf.close(f)

 %% Model levels
 f = netcdf.open(c.lev_file,'nowrite');

 varid = netcdf.inqVarID(f,'lvl');
 axes.lev.num    = netcdf.getVar(f,varid);

 varid = netcdf.inqVarID(f,'lvlp1');
 axes.lev.num_interface    = netcdf.getVar(f,varid);

 % p = a + b*psi 

 varid = netcdf.inqVarID(f,'a_model_ave');
 axes.lev.a_model_ave    = netcdf.getVar(f,varid);
 
 varid = netcdf.inqVarID(f,'a_model_alt');
 axes.lev.a_model_alt    = netcdf.getVar(f,varid);

 varid = netcdf.inqVarID(f,'a_interface');
 axes.lev.a_interface    = netcdf.getVar(f,varid);

 varid = netcdf.inqVarID(f,'b_model_ave');
 axes.lev.b_model_ave    = netcdf.getVar(f,varid);
 
 varid = netcdf.inqVarID(f,'b_model_alt');
 axes.lev.b_model_alt    = netcdf.getVar(f,varid);

 varid = netcdf.inqVarID(f,'b_interface');
 axes.lev.b_interface    = netcdf.getVar(f,varid);

 

 netcdf.close(f)



end
