function omega = get_omega(u,v,lon,lat,dm)
% This function calculates the pressure vertical velocity omega
% where eta is the models vertical coordinate for CMIP5 data on model levels
%
% omega = get_omega(lon,lat,u,v,p_i)
% 

% Get the constants (gravity and dry gas constant used)
con = atm.load_constants;


% Calculate divergence of horizontal velocity
divu = earth.div(u.*dm,v.*dm,lon,lat);

omega = zeros(size(dm));

%dp = diff(p_i,1,3);
omega = cumsum(divu.*dm,3);

% Need to deal with mass non covergence at the top...

