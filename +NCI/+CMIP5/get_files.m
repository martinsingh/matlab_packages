function [files] = get_files(varargin)
%
% Function to list the data files availible for a given scenario, model and variable in CMIP5
%
% [files] = experiment(simulation,model,freq,variable)
%
%
%
%



c = NCI.CMIP5.CMIP5_metadata;

% Defaults
simulation = 'historical';
model = 'MRI-ESM1';
freq = 'mon';
variable = 'ta';


if nargin >=1; simulation = varargin{1}; end
if nargin >=2; model = varargin{2}; end
if nargin >=3; freq = varargin{3}; end
if nargin >=4; variable = varargin{4}; end


Imodel = find(strcmp(model,c.models));
institution = c.institutions{Imodel};
ensemble = c.ensembles{Imodel};

Ivar = find(strcmp(variable,c.variables));
realm = c.realms{Ivar};


% Cant start field names with numbers
if strcmp(freq,'sixhr') 
   freq_name = '6hr';
elseif strcmp(freq,'threehr')
   freq_name = '3hr';
else
   freq_name = freq;
end

if strcmp(freq_name,'fx')
   dir_in = [c.data_loc '/' model '/' simulation '/' freq_name '/' realm '/' variable '/r0i0p0/'];
else
   dir_in = [c.data_loc  '/' model '/' simulation '/' freq_name '/' realm '/' ensemble '/' variable '/latest/'];
end

disp([dir_in '/*_' simulation '_*.nc' ])
info = dir([dir_in '/*_' simulation '_*.nc' ]); 

files = {info(:).name};


for i = 1:length(files)
   files{i} = [dir_in '/' files{i}];
end







