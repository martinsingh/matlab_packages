function ls_mask = get_orog(model_name,varargin)

simulation = 'historical';
if nargin >= 2; simulation = varargin{1}; end

c = NCI.CMIP5.CMIP5_metadata;
model = NCI.CMIP5.model_info(model_name);

ls_file = model.(simulation).r0i0p0.fx.orog.files;

ls_mask = ncread(ls_file{1},'orog');

end
