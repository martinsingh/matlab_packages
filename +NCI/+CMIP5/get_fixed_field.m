function field = get_fixed_field(simulation,model,var)


 md = NCI.CMIP5.CMIP5_metadata;


 file = NCI.CMIP5.get_files(simulation,model,'fx',var);
 file = file{1};

 f = netcdf.open(file,'nowrite');
 varid = netcdf.inqVarID(f,var);

 field    = netcdf.getVar(f,varid);

 field = double(field);
 field(abs(field-1e20)<1e13) = nan;

 netcdf.close(f)

end





