function [success,models_yes,models_no,models_out] = check_data(models,simulations,ensembles,freq,variables,varargin)
% Check whether you have the correct data to do a given calculation
%
% models = cell array with list of models
% simulations = cell array with list of simulations
% ensembles = either the ensemble number we want or a cell array with list of ensembles FOR EACH MODEL AND SIMULATION
% freq = the frequency we are interested in
% variables = cell array with list of varaibles we need

test_timeseries = 0;
if nargin > 5; test_timeseries = varargin{1}; end


success = 1;
models_yes = {};
models_no = {' '};
models_out.name = models;
models_out.success = ones(length(models),1);
models_out.error = repmat({' '},length(models),1);
models_out.ensembles = repmat({' '},numel(models),numel(simulations));

if test_timeseries > 1
    models_out.num_times = zeros(numel(models),numel(simulations));
end

md = NCI.CMIP5.CMIP5_metadata;

for mm = 1:length(models)
    model_name = models{mm};
    
    for ss = 1:length(simulations)
        simulation = simulations{ss};
        



        % Get the years we require        
        years = md.simulations.years{strcmp(md.simulations.name,simulation)};
        time_range = [datenum([years(1),1,1]) datenum([years(2)+1,1,1])];
        
        % Make a daily timeseries for the days we need
        days_required = time_range(1):time_range(2)-1;


 
        disp(['Checking: ' model_name ', ' simulation])
        model = NCI.CMIP5.model_info(model_name);
                    
        if ischar(ensembles)
            if strcmp(ensembles,'daily_data')
                if isfield(model.(simulation),'daily_data')
                    ensemble = {model.(simulation).daily_data};
                else
                    ensemble = {'r1i1p1'};
                end
            else
               ensemble = {ensembles};
            end
        elseif numel(ensembles) > 1
            ensemble = ensembles{mm,ss};
        end
        
        models_out.ensembles{mm,ss} = ensemble;
        for ee = 1:numel(ensemble)
            ens = ensemble{ee};

            
            try
                model = model.(simulation).(ens).(freq);
            catch
                success = 0;
                if ~strcmp(models_no{end},model_name); models_no = [models_no {model_name}]; end
                models_out.success(mm) = 0;
                models_out.error{mm} = [models_out.error{mm} 'no data: ' simulation ', ' ens ', '];
                continue % Go to next ensemble member
            end
            
            for vk = 1:length(variables)
                
                try 
                  time_period = model.(variables{vk}).time_period;
                catch
                    success = 0;
                    if ~strcmp(models_no{end},model_name); models_no = [models_no {model_name}]; end
                    models_out.success(mm) = 0;
                    models_out.error{mm} = [models_out.error{mm} 'no var: ' simulation ', ' ens ', ' variables{vk} ', '];
                    continue % Go to next variable
                end

 
		% Figure out the range of days covered by the data (even if the frequency is not daily)
	 	days_covered = [];
                for i = 1:size(time_period,1)
                     days_covered = [days_covered time_period(i,1):time_period(i,2)];
                end
                if numel(days_required) ~= sum(ismember(days_required,days_covered)) 
                    success = 0;
                    if ~strcmp(models_no{end},model_name); models_no = [models_no {model_name}]; end
                    models_out.success(mm) = 0;
                    models_out.error{mm} = [models_out.error{mm} 'missing times: ' simulation ', ' ens ', ' variables{vk} ', '];
                    continue % Go to next variable 
                end
                % Get the time vector for this variable
                if test_timeseries 

                  try
                    [time_vector,time_indices,time_files] = NCI.CMIP5.get_time_vector(model,variables{vk},time_range);
                    
                    % Add the time vector to the model structure
                    model.(variables{vk}).time_indices = time_indices;
                    model.(variables{vk}).time_files = time_files;
                    model.(variables{vk}).time_vector = time_vector;
                  catch
                    success = 0;
                    if ~strcmp(models_no{end},model_name); models_no = [models_no {model_name}]; end
                    models_out.success(mm) = 0;
                    models_out.error{mm} = [models_out.error{mm} 'data load error: ' simulation ', ' ens ', ' variables{vk} ', '];
                    continue % Go to next variable
                  end
                
                  if ~isequal(model.(variables{1}).time_vector,model.(variables{vk}).time_vector)
                    success = 0;
                    if ~strcmp(models_no{end},model_name); models_no = [models_no {model_name}]; end
                    models_out.success(mm) = 0;
                    models_out.error{mm} = [models_out.error{mm} 'time match error: ' simulation ', ' ens ', ' variables{vk} ', '];
                    continue
                  else
                    models_out.num_times(mm,ss) = length(time_vector);
                  end
 
                end


            end
            
        end
        
        
    end
    
end

I = models_out.success==1;
models_yes = models_out.name(I);

end

