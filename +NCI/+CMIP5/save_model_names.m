function save_model_names


md = NCI.CMIP5.CMIP5_metadata;
this_dir = fileparts(mfilename('fullpath'));

institutions = {};
models = {};
l = 0;

for i_output = 1:length(md.data)

    
    data_dir = md.data{i_output};
    
    inst_list = futil.dirc(data_dir);



    for i_inst = 1:length(inst_list)
    
         model_list = futil.dirc([data_dir inst_list{i_inst}]); 
        
         for i_mod = 1:length(model_list)
            l = l+1;
            institutions{l} = inst_list{i_inst};
            models{l} = model_list{i_mod};
         end
         
    end

end


save([this_dir '/file_info/model_names.mat'],'models','institutions')

