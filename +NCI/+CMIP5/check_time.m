function time_vec_out = check_data(time_vec_in)
 
 F = fieldnames(time_vec_in);
 time_vec_out = time_vec_in.(F{1});

 for i = 1:length(F)
  
   time = time_vec_in.(F{i});
   if ~isequal(time,time_vec_out); disp(time_vec_in); error('time error')

 end  

end 

