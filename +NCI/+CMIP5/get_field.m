function field = get_field(file,index,var,count,stride)


 f = netcdf.open(file,'nowrite');
 varid = netcdf.inqVarID(f,var);


 if nargin ==3

   [varname,xtype,dimids] = netcdf.inqVar(f,varid);
 
   for i = 1:length(dimids)
      [dimname, nd(i)] = netcdf.inqDim(f,dimids(i));
   end 

   start = [zeros(1,length(nd)-1) index-1];
   count = [nd(1:end-1) 1];

 else
   start = zeros(size(count));
   start(end) = index-1;
 end
 
 if nargin == 5
  field    = netcdf.getVar(f,varid,start,count,stride);
 else
  field    = netcdf.getVar(f,varid,start,count);
 end
 field = double(field);
 field(abs(field-1e20)<1e13) = nan;

 netcdf.close(f)

end





