
%
% This function looks for the file indices of the data within the time ranges set in CMIP5_metadata
% for easy access later
%

   %% Settings

   % If adding, keep all the original data, otherwse overwrite it
   adding = 0;
   
   % Subset of models (higher number means fewer models)
   I_priority = 3;
   
   % Load the model metadata
   md = NCI.CMIP5.CMIP5_metadata;
   this_dir = fileparts(mfilename('fullpath'));
   
   % Simulation list (Do only subset of variables)
   sim_list = md.simulations.name;
   %sim_list = {'historical'};
   
   
   % Variable list (Do only subset of variables)
   %var_list = md.variables.name;
   var_list = {'tas'};
   
   % Frequency list (Do only subset of available frequencies
   %freq_list = md.freqs;
   freq_list = {'day','month'};

   
   %% Get the indices we require
   
   % Set the indices
   I_model  = find(md.models.priority>=I_priority);
   I_var    = find(ismember(md.variables.name,var_list));
   I_sim    = find(ismember(md.simulations.name,sim_list));
   I_freq   = find(ismember(md.frequencies.name,freq_list));




   % Loop over models
   for mm = I_model(:)  
      model_name = (md.models.name{mm});
      info_file =  [this_dir '/file_info/' model_name '.mat'];


      if exist(info_file,'file')
        load(info_file,'model')
      else
        model = struct;
        model.name = model_name;
      end

      % Loop over simulations
      for ss = I_sim(:)
        simulation = md.simulations.name{ss};
        
        exp_years = md.simulations.years{ss};
        time_bnds(1) = datenum(exp_years(1),1,1);
        time_bnds(2) = datenum(exp_years(2)+1,1,1);


        % Loop over data frequency
        for ff = I_freq(:) 
           freq = md.frequencies.name{ff};
 
           % Loop over variables
           for vv = I_var(:)
             var = md.variables.name{vv};
             


             % Get the files that we have
             files = NCI.CMIP5.get_files(simulation,model.name,freq,var);
             
             
             % If we have already have data and just want to add, skip this.
             % Also skip if there are no files
          
             try 
              if ( model.(simulation).(freq).(var).num_times ~= 0 & adding )
                skip =1;
                disp(['skipping: ' model.name ', ' simulation ', ' freq ', ' var])
              elseif length(files) ==0
                disp(['no files: ' model.name ', ' simulation ', ' freq ', ' var])
                skip=1;
              else
                skip=0;
              end
             catch
                disp('caught')
                skip = 0;
             end

             % If not skipping, lets look at the data
             if ~skip


                model.(simulation).(freq).(var).files = files;

                disp([num2str(mm) ': ' model.name ', ' simulation ', ' freq ', ' var ', files: ' num2str(length(files))]) 
                the_time = cell(size(files));
                greg_time = cell(size(files));
                corrupted = zeros(size(files));
                
                times_in = cell(size(files));
                times_in_modlev = cell(size(files));
                num_times = 0;
                num_times_modlev = 0;


                % Loop over the files
                for i = 1:length(files)
                  disp(['file no. ' num2str(i)]) 
                  file = files{i};


                  % Get the times from the name of the file
                  [t1,t2] = NCI.CMIP5.get_time_range(file); 

                  % If times are in the bounds we want
                  if t2>=time_bnds(1) & t1<=(time_bnds(2));
                    try
                      [gregorian_time, serial_time] = ncutil.time(file,'time');
                      the_time{i} = serial_time;
                      greg_time{i} = gregorian_time;

                      % If this file is on model levels
                      if ~isempty( findstr('_cf',file) ) | ~isempty( findstr('Lev',file) )
                        times_in_modlev{i} = find(serial_time>time_bnds(1) & serial_time<=(time_bnds(2)));
                        num_times_modlev = num_times_modlev+length(times_in_modlev{i});
                      else
                        times_in{i} = find(serial_time>time_bnds(1) & serial_time<=(time_bnds(2)));
                        num_times = num_times+length(times_in{i});
                      end

                    catch

                      corrupted(i) = 1;

                    end % try
                      
                  end % if
            
                end % i:files

                model.(simulation).(freq).(var).times_in = times_in;
                model.(simulation).(freq).(var).times_in_modlev = times_in_modlev;
                model.(simulation).(freq).(var).num_times = num_times;       
                model.(simulation).(freq).(var).num_times_modlev = num_times_modlev;       
                model.(simulation).(freq).(var).corrupted = corrupted;
                model.(simulation).(freq).(var).time = the_time;       
                model.(simulation).(freq).(var).greg_time = greg_time;       

                model.(simulation).(freq).(var).date_stamp = date;
                save(info_file,'model')
             end % if skipping

           end % vv:variables   
            

        end % ff:freqs

     end % ss:simulations

   end % models









