function [time_vector,time_indices,time_files] = get_time_vector(model,variable,time_range)


  % Get filenames
  files = model.(variable).files;  
  
  % Get time ranges 
  file_time_ranges = model.(variable).time_period;
  
  % Check the files we need
  files_in = file_time_ranges(:,1) < time_range(2) & file_time_ranges(:,2) >= time_range(1);
  
  time_indices = [];
  time_files = [];
  time_vector = [];
  
  for i_file = 1:length(files)
      if files_in(i_file)

          [gregorian_time, serial_time] = ncutil.time(files{i_file});
      
          % Get time in range and ensure we don't double up on times
          ind = find( serial_time >= time_range(1) & serial_time < time_range(2) & ~ismember(serial_time,time_vector) );
      
         time_indices = [time_indices; ind];
         time_files = [time_files; i_file.*ones(size(ind))];
         time_vector = [time_vector; serial_time(ind)];
      end
  end


