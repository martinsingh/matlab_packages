function const = CMIP5_metadata


%% Set the data locations
const.data     = {'/g/data/al33/replicas/CMIP5/combined/' '/g/data/rr3/publications/CMIP5/output1/'};
const.mat_data = '../../mat_data/CMIP5/';

%% Set the simulation data

const.simulations.name = {'historical','rcp85' 'amip','piControl'};
const.simulations.years = {[1971 2000],[2071 2100],[1981 2000],[401 500]};

%% Set the variable data

atm = 'atmos';
oce = 'ocean';

const.variables.name        = {'zg' 'ua' 'va' 'ta' 'hus' 'huss' 'tas' 'pr' 'ps' 'psl' 'uas' 'vas' 'rsds' 'rsus' 'rsut' 'rsdt' 'rlds' 'rlus' 'rlut' 'hfls' 'hfss' 'wap' 'sftlf' 'orog'};
const.variables.dimension   = [  3    3    3    3     3    2      2    2    2    2      2      2     2      2      2      2      2      2      2      2      2      3     2       2  ];
const.variables.realm      = { atm  atm  atm  atm   atm  atm    atm  atm  atm  atm    atm    atm   atm    atm    atm    atm    atm    atm    atm    atm    atm    atm   atm     atm };

const.frequencies.name  = {'fx' 'mon'  'day'  '6hr'      '6hr'      '3hr'     };
const.frequencies.field = {'fx' 'mon'  'day'  'sixhrlev' 'sixhr'    'threehr' };
const.frequencies.alt   = {'fx' 'Amon' 'day'  '6hrLev'   '6hrPlev' '3hr'      };

%% Set the model data

% Get the model names
this_dir = fileparts(mfilename('fullpath'));
if exist([this_dir '/file_info/model_names.mat'],'file')
    load([this_dir '/file_info/model_names.mat'],'models','institutions')

    const.models.name = models;
    const.models.institution = institutions;
    
    
    %% Create a useful subset of models that represent a range of institutions
    
    % 26 model subset (has daily tas, pr, huss, and psl)
    model_subset1 = {...
     'bcc-csm1-1'    'bcc-csm1-1-m'   'BNU-ESM'      'CanESM2'          ...
     'CNRM-CM5'      'inmcm4'         'HadGEM2-ES'   'IPSL-CM5A-LR'     ...
     'IPSL-CM5A-MR'  'IPSL-CM5B-LR'   'MIROC-ESM'    'MIROC-ESM-CHEM'   ...
     'MIROC5'        'HadGEM2-CC'     'HadGEM2-ES'   'MRI-CGCM3'        ...
     'GISS-E2-H'     'GISS-E2-R'      'CCSM4'        'NorESM1-M'        ...
     'GFDL-CM3'      'GFDL-ESM2G'     'GFDL-ESM2M'   'ACCESS1-0'        ...
     'ACCESS1-3'     'CSIRO-Mk3-6-0'};
    
    
    
    % 14 model subset (has daily ta, hus, wap in addition to above)
    model_subset2 = {    ... 
     'ACCESS1-0'     'ACCESS1-3'        'BNU-ESM'                       ...
     'CanESM2'       'CSIRO-Mk3-6-0'                                    ...
     'CNRM-CM5'      'GFDL-CM3'         'GFDL-ESM2G'                    ...
     'IPSL-CM5A-LR'  'IPSL-CM5B-LR'     'MIROC-ESM-CHEM'                ...
     'MIROC5'        'MRI-CGCM3'        'NorESM1-M'                     ...
    };

    % 5 model subset (5 models from big global centres)
    model_subset3 = {'GFDL-CM3', 'ACCESS1-0','MIROC5','IPSL-CM5A-LR','CNRM-CM5'};

    % Single model subset
    model_subset4 = {'GFDL-CM3'};

    const.models.priority = zeros(length(const.models.name),1,'int8');
    const.models.priority(ismember(const.models.name,model_subset1)) = 1;
    const.models.priority(ismember(const.models.name,model_subset2)) = 2;
    const.models.priority(ismember(const.models.name,model_subset3)) = 3;
    const.models.priority(ismember(const.models.name,model_subset4)) = 4;
else
    warning('No model name file exists: run save_model_names.m')
end
    
    
