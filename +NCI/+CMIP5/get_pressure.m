function [p,p_i,varargout] = get_pressure(model,a,a_i,b,b_i,c,ps,varargin)
% This function calculates the pressures (and heights) for CMIP5 model level data
%
% [p,p_i,[,z,z_i]] = get_pressure(model,a,a_i,b,b_i,c,ps[,Tv,z_surf])
% 
% For height calculation need Tv and z_surf
% For models on height coords (ACCESS) need Tv for pressure calculation
%
% Current models choices available
%
%	GFDL*
%	BNU*
%	MIROC*
%	NorESM*
%	ACCESS*
%
%

% Get the constants (gravity and dry gas constant used)
con = atm.load_constants;

% Extend the surface pressure matrix
ps_mat = repmat(ps,size(a(1,1,:)));

if ~isempty(findstr(model,'GFDL')) | ~isempty(findstr(model,'BNU')) | ~isempty(findstr(model,'MIROC')) | ~isempty(findstr(model,'Nor')) |  ~isempty(findstr(model,'IPSL'))  | ~isempty(findstr(model,'FGOALS')) 
% Models on hybrid sigma levels
 
ps_mat_i = repmat(ps,size(a_i(1,1,:)));

   if  ~isempty(findstr(model,'IPSL'))     	% IPSL models
      p = a + b.*ps_mat;
      p_i = a_i + b_i.*ps_mat_i;

   elseif ~isempty(findstr(model,'FGOALS'))     % FGOALS models

      p = c + b.*(ps_mat-c);
      p_i = c + b_i.*(ps_mat_i-c);
      
   else 					% Other models
      p = a.*c + b.*ps_mat;
      p_i = a_i.*c + b_i.*ps_mat_i;
   end



   % Calculate height if required
   if nargout >= 3
 
      % Get the virtual temperature, height of surface, and log pressure at interface levels
      Tv = varargin{1};
      z_surf = varargin{2};
      z_surf = repmat(z_surf,size(Tv(1,1,:)));     
      logp_i = log(p_i);
      logp = log(p);

      % Integrate to get the height on interface levels assuming hydrostasy. 
      % We also assume that the first interface level is at the surface
      z_i = z_surf - cumsum(con.Rd.*Tv./con.g.*diff(logp_i,1,3),3);
      z_i = cat(3,z_surf(:,:,1),z_i);

      % Intergrate to scalar levels
      z = z_i(:,:,1:end-1) - con.Rd.*Tv./con.g.*(logp-logp_i(:,:,1:end-1));

   end

elseif  ~isempty(findstr(model,'ACCESS')) 
% Models on height levels

   Tv = varargin{1};
 
   % get the geopotential height
   z = a;
   z_i = b;
   dz = c;



   % CAlculate pressur
   %p_i = zeros(size(z_i));
   %p_i(:,:,1) = ps_mat(:,:,1);
   %for i = 1:size(z,3)
   %  p_i(:,:,i+1) = p_i(:,:,i).*(1 - con.g./(con.Rd.*Tv(:,:,i)).*dz(:,:,i));      
   %end
   %p = (p_i(:,:,1:end-1)+p_i(:,:,2:end))./2;

   exner_s = (ps_mat./con.p00).^(con.Rd./con.cp);

   logexner_i = log(exner_s) - cumsum(con.g./(con.cp.*Tv).*dz,3);
   logexner = logexner_i - con.g./(con.cp.*Tv).*(z-z_i(:,:,1:end-1));

   % Calculate pressure
   p_i = con.p00.*exp(con.cp./con.Rd.*logexner_i);
   p_i = cat(3,ps_mat(:,:,1),p_i);

   p = con.p00.*exp(con.cp./con.Rd.*logexner);

 
   % calculate pressure
   %logp_i = log(ps_mat) - cumsum(con.g./(con.Rd.*Tv).*dz,3);
   %p_i = exp(logp_i);
   %p_i = cat(3,ps_mat(:,:,1),p_i);

   % pressure on T levels
   %logp = log(p_i(:,:,1:end-1)) - con.g./(con.Rd.*Tv).*(z-z_i(:,:,1:end-1));
   %p = exp(logp);

else
  error('Model vertical coord error')
end

if nargout >= 2; varargout{1} = z; end
if nargout >= 3; varargout{2} = z_i; end




