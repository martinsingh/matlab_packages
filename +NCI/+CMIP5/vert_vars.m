function vars = vert_vars(model)
model
if ~isempty(findstr(model,'GFDL'))
   vars = {'a' 'b' 'p0'};
elseif ~isempty(findstr(model,'ACCESS'))
   vars = {'lev' 'b' 'orog'};

else
   error('do not know the vertical coordinate variables for this model')
end

