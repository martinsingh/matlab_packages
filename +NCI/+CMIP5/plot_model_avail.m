

c = NCI.CMIP5.CMIP5_metadata;

this_dir = fileparts(mfilename('fullpath'));

%% Find out the simulations, models, and variables we have data for at daily and monthly resolution

model_avail = struct;
model_avail.fx = zeros(length(c.models.name),length(c.simulations.name),length(c.variables.name));
model_avail.mon = zeros(length(c.models.name),length(c.simulations.name),length(c.variables.name));
model_avail.day = zeros(length(c.models.name),length(c.simulations.name),length(c.variables.name));
model_avail.sixhr = zeros(length(c.models.name),length(c.simulations.name),length(c.variables.name));
model_avail.sixhrlev = zeros(length(c.models.name),length(c.simulations.name),length(c.variables.name));
model_avail.threehr = zeros(length(c.models.name),length(c.simulations.name),length(c.variables.name));




for mm = 1:length(c.models.name)

    disp(c.models.name{mm})
    model = NCI.CMIP5.model_info(c.models.name{mm});

    %% Loop over simulations
    for ss = 1:length(c.simulations.name)
    simulation = c.simulations.name{ss};
    if isfield(model,simulation)

        % Loop over ensembles
        ensembles = fieldnames(model.(simulation));
        if numel(ensembles) > 0
        ensembles = model.(simulation).ensemble_members;
        for ee = 1:length(ensembles)
        ensemble = ensembles{ee};
       
           % Loop over frequencies
           for ff = 1:length(c.frequencies.field)
           freq = c.frequencies.field{ff};
           if isfield(model.(simulation).(ensemble),freq)
             
              % Loop over variables 
              for vv = 1:length(c.variables.name)
              var = c.variables.name{vv};
          
                 model_avail.(freq)(mm,ss,vv)      = model_avail.(freq)(mm,ss,vv)      + isfield(model.(simulation).(ensemble).(freq),var);
       
            
              end % variables
                    
            end % if frequencies
            end % frequencies
                
      
        end % ensembles  
        end % if ensembles

    end % if simulations    
    end % simulations
    
end % models



%% Now make a plot of it %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


for kkk = 1:3

if kkk ==1
   the_data = model_avail.day;
   the_string = 'Daily data, historical (green) and RCP85 (blue)';
   the_file = [this_dir '/file_info/daily_avail.pdf'];
elseif kkk==2
   the_data = model_avail.mon;
   the_string = 'Monthly data, historical (green) and RCP85 (blue)';
   the_file = [this_dir '/file_info/monthly_avail.pdf'];
else
   the_data = model_avail.sixhr;
   the_data_3hr = model_avail.threehr;
   the_data_modlev = model_avail.sixhrlev;
   the_string = 'Six-hourly data, historical (green) and RCP85 (blue), on model levs. (red squares), three hourly (red line)';
   the_file = [this_dir '/file_info/sixhr_avail.pdf'];

end

fig.bfig('a4p')


   annotation('textbox',[0.08 0.985 0.9 0.02],'string',the_string,'linestyle','none','fontsize',10);
for j = 1:length(c.variables.name)

  annotation('textbox',[0.19+(j-1)*0.035 0.97 0.0175 0.015],'string',c.variables.name{j},'linestyle','none','fontsize',7,'horizontalalignment','center');
  annotation('line',[0.18+(j-1)*0.035 0.18+(j-1)*0.035],[0.01 0.97],'linewidth',0.5)
  
end


for i = 1:length(c.models.name)

   annotation('textbox',[0.01 0.97-i*0.015 0.2 0.013],'string',num2str(i),'linestyle','none','fontsize',6);
   annotation('textbox',[0.05 0.97-i*0.015 0.2 0.013],'string',c.models.name{i},'linestyle','none','fontsize',6);
   annotation('line',[0.02 0.98],[0.9665-i*0.015 0.9665-i*0.015],'linewidth',0.5)

   for j = 1:length(c.variables.name)

     if the_data(i,1,j)>0
        annotation('rectangle',[0.19+(j-1)*0.035 0.97-i*0.015 0.007 0.01],'linestyle','none','facecolor','g')
     end


     if the_data(i,2,j)>0
       annotation('rectangle',[0.202+(j-1)*0.035 0.97-i*0.015 0.007 0.01],'linestyle','none','facecolor','b')
     end

     


     if kkk==3
         
        
        if the_data_modlev(i,1,j)>0
           annotation('rectangle',[0.19+(j-1)*0.035 0.97-i*0.015 0.007 0.01],'linewidth',1,'color','r')
        end

         
        if the_data_modlev(i,2,j)>0
           annotation('rectangle',[0.202+(j-1)*0.035 0.97-i*0.015 0.007 0.01],'linewidth',1,'color','r')
        end
         
        if the_data_3hr(i,1,j)>0
           annotation('ellipse',[0.19+(j-1)*0.035 0.97-i*0.015 0.002 0.01],'linewidth',1,'color','r')
        end
        if the_data_3hr(i,2,j)>0
           annotation('ellipse',[0.202+(j-1)*0.035 0.97-i*0.015 0.002 0.01],'linewidth',1,'color','r')
        end
     end

   end

end

print('-dpdf',the_file)
end





