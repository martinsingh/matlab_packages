
%
% This function looks for the file indices of the data within the time ranges set in CMIP5_metadata
% for easy access later
%

%% Settings

% If adding, keep all the original data, otherwse overwrite it
% replace = 0, add these simulations, replace = 1, create new structure,
% replace = -1, skip all models that exist
replace = 1;

% Subset of models (higher number means fewer models)
I_priority = 0;

% Load the model metadata
md = NCI.CMIP5.CMIP5_metadata;
this_dir = fileparts(mfilename('fullpath'));

% Simulation list (Do only subset of variables)
sim_list = md.simulations.name;


% Variable list (Do only subset of variables)
var_list = md.variables.name;

% Frequency list (Do only subset of available frequencies
freq_list = md.frequencies.name;

%% Get the indices we require

% Set the indices
I_model  = find(md.models.priority>=I_priority);
I_var    = find(ismember(md.variables.name,var_list));
I_sim    = find(ismember(md.simulations.name,sim_list));
I_freq   = find(ismember(md.frequencies.name,freq_list));


%I_model = 61;

% Loop over models
for mm = I_model'
    model_name = (md.models.name{mm});
    inst_name = (md.models.institution{mm});
    
    
    % File name for output
    info_file = [this_dir '/file_info/' model_name '.mat'];
    
    if exist(info_file) & replace == -1
       
        disp(['********** ' model_name ': exists, skipping'])
       
    else

       disp(['********** ' model_name '**********'])
       
       if replace == 0
            
            load(info_file,'model')
        
       else
    
           % Initialise Structure
           model = struct;
           model.name = model_name;
           model.inst = inst_name;
           
       end
    
       % Find location of model
       for i = 1:length(md.data)
           directory = [md.data{i} inst_name '/' model_name];
           if exist(directory,'dir')
               model.directory = directory;
           end
       end
    
    
    
    
       % Loop over simulations
       for ss = I_sim
         simulation = md.simulations.name{ss};
        
         model.(simulation) = struct;
        
         % Find the ensemble members of this simulation 
         % Assume monthly tas has all ensemble members -- also get "zero" ensemble member for fixed fields)
         ens_list = [ futil.dirc([model.directory '/' simulation '/fx/atmos/fx/']) futil.dirc([model.directory '/' simulation '/mon/atmos/Amon/']) ];
       
         if isempty(ens_list)

            disp(['*** ' simulation ': not found']) 
            model = rmfield(model,simulation);

         else 
 
            disp(['*** ' simulation])
            model.(simulation).ensemble_members = ens_list;
        
            for ee = 1:length(ens_list)
               ensemble = ens_list{ee};
               model.(simulation).(ensemble) = struct;

               disp(ensemble)
            
               % Loop over data frequency
               for ff = I_freq
                   freq = md.frequencies.field{ff};
                   freq_name = md.frequencies.name{ff};
                   freq_alt = md.frequencies.alt{ff};
                
                   model.(simulation).(ensemble).(freq) = struct;
                   disp(['    ' model_name ', ' simulation ', ' ensemble ', ' freq_name])

                
                   % Loop over variables
                   for vv = I_var
                       var = md.variables.name{vv};
                       realm = md.variables.realm{vv};

                       % Find the variable files
                       versions = futil.dirc([model.directory '/' simulation '/' freq_name '/' realm '/' freq_alt '/' ensemble '/']);
                       if isempty(version)

                          disp(['             ' var ': no vars found '])
                          
                       else


                          % Get the latest version
                          if ismember('latest',versions)
                              version = 'latest';
                          elseif isempty(versions)
                              version = 'null';
                          else
                              version = versions{end};
                          end 

                    
                     
                    
                          % Find the file names
                          files = futil.dirc([model.directory '/' simulation '/' freq_name '/' realm '/' freq_alt '/' ensemble '/' version '/' var '/' var '*.nc'],'path','full');

                          % Get some extra files for CSIRO-Mk-3-6-0
                          if isempty(files) && strcmp(model_name,'CSIRO-Mk3-6-0')  && strcmp(freq_name,'day') && strcmp(ensemble,'r1i1p1')
                              version = versions{end-1};
                              files = futil.dirc([model.directory '/' simulation '/' freq_name '/' realm '/' freq_alt '/' ensemble '/' version '/' var '/*.nc'],'path','full');
                              
                          end
                          
                          if isempty(files)

                             disp(['             ' var ': not found'])

                          else                          
   
                             disp(['             ' var])

                             model.(simulation).(ensemble).(freq).(var) = struct;
                    
                             model.(simulation).(ensemble).(freq).(var).files = files;

                             if ~strcmp(freq,'fx')
   
                                time_period = nan(length(files),2);

                               
                                
                                % Find the beginning and end time range
                                for i = 1:length(files)

                                    [t1,t2] = NCI.CMIP5.get_time_range(files{i});
                                    time_period(i,1) = t1;
                                    time_period(i,2) = t2;                      
                        
                                end
                    
                                model.(simulation).(ensemble).(freq).(var).time_period = time_period;

                             end
                             if strcmp(freq,'day')
                                 if ~isfield(model.(simulation),'daily_data')
                                     model.(simulation).daily_data = ensemble;
                                 end
                             end

                             model.(simulation).(ensemble).(freq).(var).date_stamp = date;

                          end % if variable exists

                       end % if version exists
                    
                    end % vv:variables

                  if numel(fieldnames(model.(simulation).(ensemble).(freq))) == 0
                     model.(simulation).(ensemble) = rmfield(model.(simulation).(ensemble),freq);
                  end

               end % ff:freqs
            
           end % ee:ensembles

           if strcmp(model_name,'CSIRO-Mk3-6-0') || strcmp(model_name,'CNRM-CM5')
               model.(simulation).daily_data = 'r1i1p1';
           end
           
        end % if simulation exists
        
      end % ss:simulations

   end % if model exists

   save(info_file,'model')
     
end % models









