% test script for the RK solver in this package

%% CASE 1: dydt = f(t) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Function we solve
fun = @(t,y) sin(t);

% Parameters
dt = 0.2;
yi = 1;
ti = 0;
tf = 10;


% Create vectors
t = ti:dt:tf;

% Derivative
dydt = zeros(size(t));

% Numerical solutions
y_euler = zeros(size(t));
y_midpoint = zeros(size(t));
y_rk4 = zeros(size(t));

% Set initial conditions
y_euler(1) = yi;
y_midpoint(1) = yi;
y_rk4(1) = yi;

% Exact solution
y_exact = 2-cos(t);


for i = 1:length(t)-1
   y_euler(i+1) = ODE.rk_step(fun,y_euler(i),t(i),dt,'euler');
   y_midpoint(i+1) = ODE.rk_step(fun,y_midpoint(i),t(i),dt,'midpoint');
   y_rk4(i+1) = ODE.rk_step(fun,y_rk4(i),t(i),dt,'rk4');
   dydt(i) = fun(t(i),y_exact(i));
end


fig.bfig(10,14)
fig.set_default('paper')

subplot(211)

plot(t,y_exact,'k');
hold on
plot(t,y_euler,'b');
plot(t,y_midpoint,'r');
plot(t,y_rk4,'g--');

xlabel('t')
ylabel('y')
l = legend('exact','Euler','midpoint','RK4');
box off
set(l,'box','off')
title('Solution: dy/dt = sin(t)')

subplot(212)

plot(t,abs(y_exact-y_euler),'b');
hold on
plot(t,abs(y_exact-y_midpoint),'r');
plot(t,abs(y_exact-y_rk4),'g--');
set(gca,'yscale','log')

title(['Solution error: dy/dt = sin(t), step size = ' num2str(dt)])
xlabel('t')
ylabel('y')
l = legend('Euler','midpoint','RK4');
box off
set(l,'box','off')


%% CASE 2: dydt = f(y,t) - linear %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Function we solve
fun = @(t,y) sin(t) + y;



% Parameters
dt = 0.05;
yi = 1;
ti = 0;
tf = 3;



% Create vectors
t = ti:dt:tf;

% Test against matlab in built functions
%[t_m,y_m45] = ode45(fun,t,yi);
%[t_m,y_m23] = ode23(fun,t,yi);

% Derivative
dydt = zeros(size(t));

% Numerical solutions
y_euler = zeros(size(t));
y_midpoint = zeros(size(t));
y_rk4 = zeros(size(t));
y_rk5 = zeros(size(t));

% Set initial conditions
y_euler(1) = yi;
y_midpoint(1) = yi;
y_rk4(1) = yi;
y_rk5(1) = yi;

% Exact solution
%y_exact = exp(1-cos(t));
y_exact = 3.*exp(t)./2 - (cos(t) + sin(t))./2;

for i = 1:length(t)-1
   y_euler(i+1) = ODE.rk_step(fun,y_euler(i),t(i),dt,'euler');
   y_midpoint(i+1) = ODE.rk_step(fun,y_midpoint(i),t(i),dt,'midpoint');
   y_rk4(i+1) = ODE.rk_step(fun,y_rk4(i),t(i),dt,'rk4');
   y_rk5(i+1) = ODE.rk_step(fun,y_rk5(i),t(i),dt,'rk5');

   dydt(i) = fun(t(i),y_exact(i));
end


fig.bfig(10,14)
fig.set_default('paper')

subplot(211)

plot(t,y_exact,'k');
hold on
plot(t,y_euler,'b');
plot(t,y_midpoint,'r');
plot(t,y_rk4,'g--');

xlabel('t')
ylabel('y')
l = legend('exact','Euler','midpoint','RK4');
box off
set(l,'box','off')
title('Solution: dy/dt = sin(t)*y')

subplot(212)

plot(t,abs(y_exact-y_euler),'b');
hold on
plot(t,abs(y_exact-y_midpoint),'r');
plot(t,abs(y_exact-y_rk4),'g--');
plot(t,abs(y_exact-y_rk5),'m--');
set(gca,'yscale','log')

title(['Solution error: dy/dt = sin(t) + y, step size = ' num2str(dt)])
xlabel('t')
ylabel('y')
l = legend('Euler','midpoint','RK4','RK5');
box off
set(l,'box','off')



%% CASE 3: dydt = f(y,t) - linear %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Function we solve
fun = @(t,y) sin(t).*y;

% Parameters
dt = 0.05;
yi = 1;
ti = 0;
tf = 24;


% Create vectors
t = ti:dt:tf;

% Derivative
dydt = zeros(size(t));

% Numerical solutions
y_euler = zeros(size(t));
y_midpoint = zeros(size(t));
y_rk4 = zeros(size(t));
y_rk5 = zeros(size(t));

% Set initial conditions
y_euler(1) = yi;
y_midpoint(1) = yi;
y_rk4(1) = yi;
y_rk5(1) = yi;

% Exact solution
y_exact = exp(1-cos(t));


for i = 1:length(t)-1
   y_euler(i+1) = ODE.rk_step(fun,y_euler(i),t(i),dt,'euler');
   y_midpoint(i+1) = ODE.rk_step(fun,y_midpoint(i),t(i),dt,'midpoint');
   y_rk4(i+1) = ODE.rk_step(fun,y_rk4(i),t(i),dt,'rk4');
   y_rk5(i+1) = ODE.rk_step(fun,y_rk5(i),t(i),dt,'rk5');

   dydt(i) = fun(t(i),y_exact(i));
end


fig.bfig(10,14)
fig.set_default('paper')

subplot(211)

plot(t,y_exact,'k');
hold on
plot(t,y_euler,'b');
plot(t,y_midpoint,'r');
plot(t,y_rk4,'g--');

xlabel('t')
ylabel('y')
l = legend('exact','Euler','midpoint','RK4');
box off
set(l,'box','off')
title('Solution: dy/dt = sin(t)*y')

subplot(212)

plot(t,abs(y_exact-y_euler),'b');
hold on
plot(t,abs(y_exact-y_midpoint),'r');
plot(t,abs(y_exact-y_rk4),'g--');
plot(t,abs(y_exact-y_rk5),'m--');
set(gca,'yscale','log')

title(['Absolute solution error: dy/dt = sin(t)*y, step size = ' num2str(dt)])
xlabel('t')
ylabel('y')
l = legend('Euler','midpoint','RK4','RK5');
box off
set(l,'box','off')

%% CASE 4: Vector %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Function we solve
fun = @(t,y) [sin(t); sin(t)];

% Parameters
dt = 0.2;
yi = [1; 1];
ti = 0;
tf = 10;


% Create vectors
t = ti:dt:tf;

% Derivative
dydt = zeros(length(yi),length(t));

% Numerical solutions
y_euler = zeros(size(dydt));
y_midpoint = zeros(size(dydt));
y_rk4 = zeros(size(dydt));

% Set initial conditions
y_euler(:,1) = yi;
y_midpoint(:,1) = yi;
y_rk4(:,1) = yi;

% Exact solution
y_exact = 2-cos(t);


for i = 1:length(t)-1
   y_euler(:,i+1) = ODE.rk_step(fun,y_euler(:,i),t(i),dt,'euler');
   y_midpoint(:,i+1) = ODE.rk_step(fun,y_midpoint(:,i),t(i),dt,'midpoint');
   y_rk4(:,i+1) = ODE.rk_step(fun,y_rk4(:,i),t(i),dt,'rk4');
   
end



fig.bfig(10,14)
fig.set_default('paper')

subplot(211)

plot(t,y_exact,'k');
hold on
plot(t,y_euler,'b');
plot(t,y_midpoint,'r');
plot(t,y_rk4,'g--');

xlabel('t')
ylabel('y')
l = legend('exact','Euler','midpoint','RK4');
box off
set(l,'box','off')
title('Solution: dy/dt = sin(t)')

subplot(212)

plot(t,abs(y_exact-y_euler),'b');
hold on
plot(t,abs(y_exact-y_midpoint),'r');
plot(t,abs(y_exact-y_rk4),'g--');
set(gca,'yscale','log')

title(['Solution error: dy/dt = sin(t), step size = ' num2str(dt)])
xlabel('t')
ylabel('y')
l = legend('Euler','midpoint','RK4');
box off
set(l,'box','off')

